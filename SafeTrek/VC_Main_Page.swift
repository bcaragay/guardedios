//
//  VC_Main_Page.swift
//  Guarded
//
//  Created by Bryan Caragay on 2/10/16.
//  Copyright © 2016 Bryan Caragay. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import MessageUI
import Firebase
import Intents
import GooglePlaces
import GoogleMaps
import AVFoundation

class VC_Main_Page: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate, UITableViewDataSource, UITableViewDelegate,GMSMapViewDelegate{

    var homeGeo : Geotification!
    
    @IBOutlet var emergencyAlertView: UIView!
    
    //Used for live demo testing.
    var recordData = true
    
    //Design of Destiation Alert Maps
    @IBOutlet var mapContainer: UIView!
    
    //Label used to post alerts at the top of the app.
    @IBOutlet var lsAlert: UILabel!
    @IBOutlet var topBar: UIView!
    
    //Destination Alert view
    @IBOutlet var goHomePopup: UIView!

    /// Building finder components
    var effect:UIVisualEffect!
    @IBOutlet var blurEffectView: UIVisualEffectView!
    @IBOutlet var popupView: PopupViewController!
    @IBOutlet var aButton: UIButton!
    var curAnnotation: GMSMarker?
    var shouldCenter = true
    
    //Used for destionation alerts to display where the user is going
    var circ : GMSCircle?
    var homeCircle : GMSCircle?

    //Used to determine the duration the app is being used for
    var lastTime = NSDate.timeIntervalSinceReferenceDate
    
    //Collects user location when held down
    var locationDict = [String: [String : String]]()
    
    //Preloading the contacts array
    var contacts: [String] = []
    
    /// Usecd to gather organization specific information
    var org = ""
    var hotline1Name = "Error Loading Hotline"
    var hotline1Num = ""
    var hotline2Name = "Error Loading Hotline"
    var hotline2Num = ""
    var reportHotline = ""
    
    /// USER VARS FOR TEXT MESSAGE
    var name : String!
    var phoneNum = ""
    var uid = ""
    var fakecallerid = "Mom"
    
    /// Firebase Data
    let ref = Database.database().reference()
    var handle = DatabaseHandle()

    ///Map and location variables
    var heldDownLocation: CLLocation!
    var lastLocation : CLLocation!
    var coordinates: [CLLocationCoordinate2D] = []
    @IBOutlet var gMapViews: GMSMapView!
    var locationManager: CLLocationManager!
    var location : CLLocation!
    var homeCoord : CLLocation!
    var lastHomeCoord : CLLocationCoordinate2D!
    @IBOutlet var goHomeMap: GMSMapView!
    @IBOutlet var goHomeName: UIButton!
    
    //Is the main button down?
    var heldDown = false
    
    //Added to lockdown the app during emergency situations
    @IBOutlet var lockdownLabel: UIView!
    
    //================================================================================
    //# MARK: - Setup Methods
    //================================================================================
    
    override func viewWillAppear(_ animated: Bool) {
        setupUI()
        
//        ref.child("Announcements").child(org).observeSingleEvent(of: .value, with: { (snapshot) in
//            if(snapshot.hasChildren()){
//
//                print(snapshot.childSnapshot(forPath: "Title").value!)
//                print(snapshot.childSnapshot(forPath: "Body").value!)
//            }
//        })

        //Check for Campus Lockdown Mode
        self.ref.child("/Lockdown/" + org).observeSingleEvent(of: .value, with: { (snapshot) in
            if(snapshot.exists()){
                let lockdown = Bool(snapshot.value as! Bool)
                print("Lockdown: " + lockdown.description)
                //Lockdown is in effect
                if(lockdown){
                    self.lockdownLabel.frame = self.topBar.bounds
                    self.lockdownLabel.autoresizingMask = [.flexibleWidth, .flexibleHeight]
                    self.topBar.addSubview(self.lockdownLabel)
                
                    self.navigationItem.leftBarButtonItem?.title = "Updates"
                }
            }
        })
        
        //Hide the back button, We don't want the user going back to the log in screen without logging out
        self.navigationItem.setHidesBackButton(true, animated:true);
        
        //Displays or hides the red alert bar.
        checkPerms()

        do {
            // Set the map style by passing the URL of the local file.
            if let styleURL = Bundle.main.url(forResource: "style", withExtension: "json") {
                gMapViews.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                //The map style couldn't be loaded. Defaulting to the default style.
            }
        } catch {
            //The map style couldn't be loaded. Defaulting to the default style.
        }
        
        if let user = Auth.auth().currentUser{
            uid = user.uid
        }
        
        //Building finder stuff
        popupView.tableView.delegate = self
        popupView.tableView.dataSource = self
        
        let contactsRef = ref.child("/users/").child(org).child(self.uid + "/contacts")
        
        contacts = []
        
        //Adding the contacts to the array, to be able to access names and numbers.
        contactsRef.queryOrderedByKey().observe(.childAdded, with: { snapshot in
            if(snapshot.exists()){
                let c = Contact(name: (snapshot.key), num: (snapshot.value as? String)!)
                self.contacts.append(c.name)
            }
        })

        if let user = Auth.auth().currentUser{
            //Getting the users name
            ref.child("/users/").child(org).observeSingleEvent(of: .value, with: { (snapshot) in
                
                if snapshot.hasChild(user.uid){
                    
                    let v = snapshot.childSnapshot(forPath: user.uid).value as! [String:AnyObject]
                
                    if let name = v["full_name"] as? String{
                        self.name = name
                        UserDefaults.standard.set(name, forKey: "Username")
                    }
                    if let phonenumber = v["phone_number"] as? String{
                        self.phoneNum = phonenumber
                    }
                    if let callerid = v["callerid"] as? String{
                        self.fakecallerid = callerid
                    }
                }else{
                    print("No user data found")
                    let alert = SCLAlertView()
                
                    alert.showError("No user data found", subTitle: "Please contact an administrator.")
                    
                    try! Auth.auth().signOut()
                    _ = self.navigationController?.popViewController(animated: true)
                }
            })
        }

        //Get current user UID
        if let user = Auth.auth().currentUser{
            uid = user.uid
        }
    
        goHomeMap.isUserInteractionEnabled = false
        
        if(isKeyPresentInUserDefaults(key: "HomeLocationLat") ){
            let lat = UserDefaults.standard.float(forKey: "HomeLocationLat")
            let long = UserDefaults.standard.float(forKey: "HomeLocationLong")
            setupHomeLocation(lat: lat, long: long)
        }
    }
    
    func setupUI(){
        //Add the blur effect to the main View
        let blurEffect = UIBlurEffect(style: .regular)
        blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.view.frame
        self.view.addSubview(blurEffectView)
        self.blurEffectView.isHidden = true
        
        //Adding the drop shadow to the main alert button
        aButton.layer.shadowColor = UIColor.black.cgColor
        aButton.layer.shadowOffset = CGSize(width: 0, height: 5)
        aButton.layer.shadowRadius = 5
        aButton.layer.shadowOpacity = 0.8
        
        if(isKeyPresentInUserDefaults(key: "GoHomeUserName")){
            //Set the name of the person to alert when using the destination alert.
            let key = UserDefaults.standard.string(forKey: "GoHomeUserName")
            goHomeName.setTitle(key?.description, for: .normal)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        mapContainer.layer.shadowColor = UIColor.black.cgColor
        mapContainer.layer.shadowOffset = CGSize(width: 0, height: 0)
        mapContainer.layer.shadowRadius = 3
        mapContainer.layer.shadowOpacity = 0.5
        goHomeMap.layer.masksToBounds = true
    }
    
    func setupHomeLocation(lat: Float, long: Float){
        let coord = CLLocationCoordinate2D(latitude: CLLocationDegrees(lat), longitude: CLLocationDegrees(long))
        addGeotification(coordinate: coord , radius: 20, identifier: "Test", note: "Testing", eventType: .onEntry)
        
        print("Setting Up Home Location.")
        //Set the Go Home Popup map to show the home location
        homeCoord = CLLocation(latitude: CLLocationDegrees(lat), longitude: CLLocationDegrees(long))
        let adjCoord = CLLocation(latitude: homeCoord.coordinate.latitude - 0.0001, longitude: homeCoord.coordinate.longitude)
        goHomeMap.camera = GMSCameraPosition(target: adjCoord.coordinate, zoom: 16, bearing: 0, viewingAngle: 0)
        homeCircle?.map = nil
        homeCircle = GMSCircle(position: homeCoord.coordinate, radius: 25)
        homeCircle?.strokeColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        homeCircle?.fillColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 0.5518716449)
        homeCircle?.map = goHomeMap
    }
    
    /// Adding the building labels to the map
    func addOverlays(lat: Float, long: Float, lat2: Float, long2: Float, img: String){
        let southWest = CLLocationCoordinate2D(latitude: CLLocationDegrees(lat), longitude: CLLocationDegrees(long))
        let northEast = CLLocationCoordinate2D(latitude: CLLocationDegrees(lat2), longitude: CLLocationDegrees(long2))
        let overlayBounds = GMSCoordinateBounds(coordinate: southWest, coordinate: northEast)
        
        let icon = UIImage(named: img)
        
        let overlay = GMSGroundOverlay(bounds: overlayBounds, icon: icon)
        overlay.bearing = 0
        overlay.map = gMapViews
        
        gMapViews.delegate = self
        gMapViews.padding = UIEdgeInsets(top: 0, left: 0, bottom: 50, right: 20)

    }
    
    @discardableResult
    func checkPerms() -> Bool{
        //Check to see if the user granted location permissions. Otherwise, disable certain features.
        if(CLLocationManager.authorizationStatus() == .authorizedAlways){
            aButton.isHidden = false

            if(isKeyPresentInUserDefaults(key: "goingHome")){
                if(UserDefaults.standard.integer(forKey: "goingHome") == 0){
                    lsAlert.backgroundColor = #colorLiteral(red: 1, green: 0, blue: 0.04472958599, alpha: 1)
                    lsAlert.text = "Your location services are not on! Cannot pinpoint your location."
                    lsAlert.isHidden = true
                }else{
                    lsAlert.backgroundColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
                    lsAlert.text = "Traveling to destination."
                    lsAlert.isHidden = false
                }
            }else{
                lsAlert.isHidden = true;
            }
            return true
        }else{
            //Hide these features because they're useless without location data.
            aButton.isHidden = true
            lsAlert.isHidden = false;
            return false
        }
    }
    
    @objc func checkAction(sender : UITapGestureRecognizer) {
        self.performSegue(withIdentifier: "homeMap", sender: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let user = Auth.auth().currentUser{
            uid = user.uid
        }
        
        emergencyAlertView.alpha = 0
        popupView.alpha = 0
        goHomePopup.alpha = 0
        
        self.view.addSubview(emergencyAlertView)
        self.view.addSubview(popupView)
        self.view.addSubview(goHomePopup)

        let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.checkAction(sender:)))
        self.goHomePopup.addGestureRecognizer(gesture)
        
        ref.child("/phoneNumbers/" + org).observeSingleEvent(of: .value, with: { (snapshot) in
            if(snapshot.exists()){
                let h1 = snapshot.childSnapshot(forPath: "hotline1").value as! [String:AnyObject]
                self.hotline1Name = self.org.capitalized + " " + (h1["title"] as! String)
                self.hotline1Num = (h1["number"]?.description)!
                
                let h2 = snapshot.childSnapshot(forPath: "hotline2").value as! [String:AnyObject]
                self.hotline2Name = self.org.capitalized + " " + (h2["title"] as! String)
                self.hotline2Num = (h2["number"]?.description)!
                
//                let h3 = snapshot.childSnapshot(forPath: "incidentHotline").value as! [String:AnyObject]
//                self.reportHotline = (h3["number"]?.description)!
            }
        })
        
        if(recordData){
            //Logging the Daily Uses when the view loads.
            ref.child("/DataCollection/" + org + "/TotalUses").observeSingleEvent(of: .value, with: { (snapshot) in
                if(snapshot.exists()){
                    var currentUses = Int(snapshot.value as! Int)
                    currentUses += 1
                    
                    let newUses = ["TotalUses" : currentUses]
                    self.ref.child("DataCollection").child(self.org).updateChildValues(newUses)
                }
            })
            
            //Logging the Daily Uses for the specific user
            ref.child("users").child(org).child(self.uid).child("DataCollection").child("TotalUses").observeSingleEvent(of: .value, with: { (snapshot) in
                if(snapshot.exists()){
                    var currentUses = Int(snapshot.value as! Int)
                    currentUses += 1
                    
                    let newUses = ["TotalUses" : currentUses]
                    self.ref.child("/users/").child(self.org).child(self.uid).child("DataCollection").updateChildValues(newUses)
                }else{
                    let newUses = ["TotalUses" : 1]
                    self.ref.child("/users/").child(self.org).child(self.uid).child("DataCollection").updateChildValues(newUses)
                }
            })
        }
        let ref4 = ref.child("/users/").child(org).child(self.uid + "/contacts")
    
        //Updates if a phone number was added
        ref4.queryOrderedByValue().observe(.value, with: { snapshot in
            if(snapshot.exists()){
                let value = snapshot.value as! NSDictionary
                if((value["Number"] as? String) != nil){
                    let c = value["Number"] as? String
                    self.contacts.append(c!)
                    print(c!)
                }
            }
        })
    
        //Adding button functions for hold, release
        aButton.addTarget(self, action: #selector(VC_Main_Page.holdRelease(_:)), for: UIControlEvents.touchUpInside);
        aButton.addTarget(self, action: #selector(VC_Main_Page.holdRelease(_:)), for: UIControlEvents.touchUpOutside)
        aButton.addTarget(self, action: #selector(VC_Main_Page.HoldDown(_:)), for: UIControlEvents.touchDown)
    
        //Initializing the location manager
        initializeLocationManager()
        
        //Adding the overlay image
        addOverlays(lat: 43.593429,  long: -84.780296, lat2: 43.594890, long2: -84.778858, img: "upperCampus")
        addOverlays(lat: 43.575300,  long: -84.786207, lat2: 43.582224, long2: -84.769128, img: "lowerCampus")
        addOverlays(lat: 43.582558,  long: -84.7807860, lat2: 43.593423, long2: -84.767902, img: "campusMap")
    }
    
    //================================================================================
    //# MARK: - Main Button
    //================================================================================

    //When the user presses the main alert button
    @objc func HoldDown(_ sender:UIButton){
        heldDown = true
        if #available(iOS 10.0, *) {
            let generator = UIImpactFeedbackGenerator(style: .heavy)
            generator.impactOccurred()
        }
        
        lastTime = NSDate.timeIntervalSinceReferenceDate
        
        if(recordData){
            self.ref.child("/DataCollection/" + org + "/MainButton").observeSingleEvent(of: .value, with: { (snapshot) in
                if(snapshot.exists()){
                    var currentUses = Int(snapshot.value as! Int)
                    currentUses += 1
                    
                    let newUses = ["MainButton" : currentUses]
                    self.ref.child("DataCollection").child(self.org).updateChildValues(newUses)
                }
            })
    
            //Logging the Daily Uses for the specific user
            ref.child("users").child(org).child(self.uid).child("DataCollection").child("MainButton").observeSingleEvent(of: .value, with: { (snapshot) in
                if(snapshot.exists()){
                    var currentUses = Int(snapshot.value as! Int)
                    currentUses += 1
                    
                    let newUses = ["MainButton" : currentUses]
                    self.ref.child("/users/").child(self.org).child(self.uid).child("DataCollection").updateChildValues(newUses)
                }else{
                    let newUses = ["MainButton" : 1]
                    self.ref.child("/users/").child(self.org).child(self.uid).child("DataCollection").updateChildValues(newUses)
                }
            })
        }
        
        heldDownLocation = lastLocation
        
        sender.setImage(UIImage(named: "ButtonHighlighted"), for: UIControlState())
        
        //Animate the main button to grow instead of snapping to size.
        UIView.animate(withDuration: 0.4, animations:{
            self.aButton.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        })
        
        //We have to disable this, because the phone would go Idle if the use just held their finger without moving it.
        UIApplication.shared.isIdleTimerDisabled = true
    }
    
    //When the user releases the main alert button
    @objc func holdRelease(_ sender:UIButton){
        heldDown = false
        
        //Set the up and down locations along with gender
        let downLat = heldDownLocation.coordinate.latitude.description
        let downLong = heldDownLocation.coordinate.longitude.description
        let upLat = lastLocation.coordinate.latitude.description
        let upLong = lastLocation.coordinate.longitude.description

        let newData = ["Down" : downLat + "," + downLong,
                       "Up":    upLat + "," + upLong,
                       "Gender":"Female"]
        
        self.ref.child("LocationCollection").child(org).child(getCurrentLocalDate()).setValue(newData)

        //Update the heatmap data
        self.ref.child("HeatmapData").child(org).updateChildValues(locationDict)

        
        //Set the time the user had the button held down.
        let time = NSDate.timeIntervalSinceReferenceDate - lastTime
        
        if(recordData){
            self.ref.child("/DataCollection/" + org + "/UsageTime").observeSingleEvent(of: .value, with: { (snapshot) in
                if(snapshot.exists()){
                    var currentUses = Double(snapshot.value as! Double)
                    currentUses += time
                    
                    let newUses = ["UsageTime" : currentUses]
                    self.ref.child("DataCollection").child(self.org).updateChildValues(newUses)
                }
            })
        }
        
        sender.setImage(UIImage(named: "Button"), for: UIControlState())
        self.aButton.transform = CGAffineTransform(scaleX: 1, y: 1)
        UIApplication.shared.isIdleTimerDisabled = false
        self.performSegue(withIdentifier: "enterCode", sender: self)
    }
    
    func getCurrentLocalDate()-> String {
        var now = Date()
        var nowComponents = DateComponents()
        let calendar = Calendar.current
        nowComponents.year = (Calendar.current as NSCalendar).component(NSCalendar.Unit.year, from: now)
        nowComponents.month = (Calendar.current as NSCalendar).component(NSCalendar.Unit.month, from: now)
        nowComponents.day = (Calendar.current as NSCalendar).component(NSCalendar.Unit.day, from: now)
        nowComponents.hour = (Calendar.current as NSCalendar).component(NSCalendar.Unit.hour, from: now)
        nowComponents.minute = (Calendar.current as NSCalendar).component(NSCalendar.Unit.minute, from: now)
        nowComponents.second = (Calendar.current as NSCalendar).component(NSCalendar.Unit.second, from: now)
        (nowComponents as NSDateComponents).timeZone = TimeZone(abbreviation: "EDT")
        now = calendar.date(from: nowComponents)!
        
        let formatter = DateFormatter()
        formatter.dateStyle = DateFormatter.Style.long
        formatter.timeStyle = .medium
        
        let dateString = formatter.string(from: now)
        return dateString
    }
    
    //================================================================================
    // END MAIN BUTTON
    //================================================================================

    //================================================================================
    //# MARK: - Map Setup
    //================================================================================
    
    //Adding annotations to the map with custom icons instead of the default Apple Maps ones
    //This is sorta depricated as we don't use it actively, but it's kinda cool so it's staying for now.
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if (annotation is MKUserLocation) {
            return nil
        }
        
        let reuseID = "saferides"
        var v = mapView.dequeueReusableAnnotationView(withIdentifier: reuseID)
        
        if v != nil {
            v!.annotation = annotation
        } else {
            v = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseID)
            v?.canShowCallout = true

            if(annotation is TrackUserAnnotation){
                let a = MKPointAnnotation()
                a.title = "Last Location"
                a.subtitle = "Tesing"
                
                let cpa = annotation as! TrackUserAnnotation
                v = MKAnnotationView(annotation: a, reuseIdentifier: reuseID)
                v?.canShowCallout = true
                v?.image = UIImage(named:cpa.imageName)
                v?.frame.size = CGSize(width: 40, height: 40)
                v?.centerOffset = CGPoint(x: 0, y: -15)
                v?.canShowCallout = true
                
                return v
            }else{
                v!.image = UIImage(named:"marker_icon")
            }
        }
        
        v?.canShowCallout = true
        
        return v
    }

    //Re-center the map and turn 'shouldCenter' on.
    @IBAction func resetPosition(_ sender: AnyObject) {
        if(checkPerms()){
            shouldCenter = true
            gMapViews.camera = GMSCameraPosition(target: lastLocation.coordinate, zoom: 18, bearing: 0, viewingAngle: 0)
        }else{
            let alert = SCLAlertView()
            alert.showError("Location Services not enabled", subTitle: "Please enable location services! Settings -> Privacy -> Location Services -> Guarded")
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        checkPerms()
        
        if status == .authorizedWhenInUse {
            locationManager.startUpdatingLocation()
            gMapViews.isMyLocationEnabled = true
            gMapViews.settings.myLocationButton = true
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            if(heldDown){
                //Updating location info for heatmap data
                let loc = ["lat": location.coordinate.latitude.description, "long": location.coordinate.longitude.description]
                locationDict[UUID().uuidString] = loc
            }
            
            if(shouldCenter){
                gMapViews.camera = GMSCameraPosition(target: location.coordinate, zoom: 18, bearing: 0, viewingAngle: 0)
                lastLocation = location
            }
        }
    }
    
    //================================================================================
    // END MAP SETUP
    //================================================================================

    //================================================================================
    //# MARK: - Assorted Methods
    //================================================================================
    @IBAction func goToOptions(_ sender: Any) {
        self.performSegue(withIdentifier: "options", sender: self)
    }

    //Switch to fake call scene
    override func motionEnded(_ motion: UIEventSubtype, with event: UIEvent?) {
        if(!heldDown){
            self.performSegue(withIdentifier: "call", sender: self)
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //Transition to the "send alert" scene
        if(segue.identifier == "enterCode"){
            let vc : VC_Send_Alert = segue.destination as! VC_Send_Alert
            vc.org = self.org
            vc.contacts = self.contacts
        }else if(segue.identifier == "options"){
            let vc : VC_Option = segue.destination as! VC_Option
            
            //Load the Option Page info from Firebase before the page loads.
            vc.hotline1Title = hotline1Name
            vc.hotline1Num = hotline1Num
            vc.hotline2Title = hotline2Name
            vc.hotline2Num = hotline2Num
           
            vc.org = self.org
       
            
        }else if(segue.identifier == "call"){
            if(recordData){
                ref.child("/DataCollection/" + self.org + "/FakeCalls").observeSingleEvent(of: .value, with: { (snapshot) in
                    if(snapshot.exists()){
                        var currentUses = Int(snapshot.value as! Int)
                        currentUses += 1
                        
                        let newUses = ["FakeCalls" : currentUses]
                        self.ref.child("DataCollection").child(self.org).updateChildValues(newUses)
                    }
                })
                
                //Logging the Daily Uses for the specific user
                ref.child("users").child(org).child(self.uid).child("DataCollection").child("FakeCalls").observeSingleEvent(of: .value, with: { (snapshot) in
                    if(snapshot.exists()){
                        var currentUses = Int(snapshot.value as! Int)
                        currentUses += 1
                        
                        let newUses = ["FakeCalls" : currentUses]
                        self.ref.child("/users/").child(self.org).child(self.uid).child("DataCollection").updateChildValues(newUses)
                    }else{
                        let newUses = ["FakeCalls" : 1]
                        self.ref.child("/users/").child(self.org).child(self.uid).child("DataCollection").updateChildValues(newUses)
                    }
                })
            }
            
            let vc : CallViewController = segue.destination as! CallViewController
            vc.name = self.fakecallerid
        }else if(segue.identifier == "goHomeContact"){
            let vc : SearchContacts = segue.destination as! SearchContacts
            vc.setHomeFlag = true
        }
    }
    
    //================================================================================
    // END ASSORTED METHODS
    //================================================================================
    
    //================================================================================
    //# MARK: - Building Finder
    //================================================================================
    @IBAction func showTableView(){
        print(popupView.alpha)
        
        if(popupView.alpha == 0){
            popupView.center = self.view.center
            self.blurEffectView.isHidden = false
            
            self.view.bringSubview(toFront: popupView)
            
            popupView.alpha = 1
        }else{
            closePopup(self)
            self.blurEffectView.isHidden = true
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return popupView.buildings.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        popupView.alpha = 0
        self.blurEffectView.isHidden = true

        let pos = moveCamera(building: (tableView.cellForRow(at: indexPath)?.textLabel?.text)!)
        
        curAnnotation?.map = nil
        let latlong = pos.components(separatedBy: ",")
        let position = CLLocationCoordinate2D(latitude: CLLocationDegrees(latlong[0])!, longitude: CLLocationDegrees(latlong[1])!)
        curAnnotation = GMSMarker(position: position)
        
        curAnnotation?.title = (tableView.cellForRow(at: indexPath)?.textLabel?.text)!

        curAnnotation?.icon = UIImage(named: "marker_small")
        
        curAnnotation?.map = gMapViews
    }
    
    @IBAction func closePopup(_ sender: Any) {
        popupView.alpha = 0
        self.blurEffectView.isHidden = true
    }
    
    @IBAction func hideMarkers(_ sender: Any) {
        curAnnotation?.map = nil

    }
    
    @IBOutlet var hideMarkers: UIButton!
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = popupView.tableView.dequeueReusableCell(withIdentifier: "cell")
        popupView.buildings.sort(){$0 < $1}

        cell?.textLabel?.text =  popupView.buildings[indexPath.row]
        cell?.accessoryType = .disclosureIndicator
        return cell!
    }
    
    func moveCamera(building: String) -> String{
        var lat = Float()
        var long = Float()
        
        if(recordData){
        
            ref.child("/DataCollection/" + org + "/BuildingFinder").observeSingleEvent(of: .value, with: { (snapshot) in
                if(snapshot.exists()){
                    var currentUses = Int(snapshot.value as! Int)
                    currentUses += 1
                    
                    let newUses = ["BuildingFinder" : currentUses]
                    self.ref.child("DataCollection").child(self.org).updateChildValues(newUses)
                }
            })
            
            //Logging the Daily Uses for the specific user
            ref.child("users").child(org).child(self.uid).child("DataCollection")
            .child("BuildingFinder").observeSingleEvent(of: .value, with: { (snapshot) in
                if(snapshot.exists()){
                    var currentUses = Int(snapshot.value as! Int)
                    currentUses += 1
                    
                    let newUses = ["BuildingFinder" : currentUses]
                    self.ref.child("/users/").child(self.org).child(self.uid).child("DataCollection").updateChildValues(newUses)
                }else{
                    let newUses = ["BuildingFinder" : 1]
                    self.ref.child("/users/").child(self.org).child(self.uid).child("DataCollection").updateChildValues(newUses)
                }
            })
        }
        
        switch(building){
        case "Anspach":
            lat = 43.587598
            long = -84.776724
            break
        case "Art Gallery":
            lat = 43.590097
            long = -84.774577
            break
        case "Barnes":
            lat = 43.590467
            long = -84.777205
            break
        case "Beddow":
            lat = 43.582874
            long = -84.776628
            break
        case "Brooks":
            lat = 43.587855
            long = -84.774740
            break
        case "Calkins":
            lat = 43.591913
            long = -84.779227
            break
        case "Carey":
            lat = 43.583872
            long = -84.779415
            break
        case "Carlin Alumni House":
            lat = 43.593098
            long = -84.770631
            break
        case "Cobb":
            lat = 43.584681
            long = -84.779437
            break
        case "Combined Services":
            lat = 43.585675
            long = -84.770614
            break
        case "University Center":
            lat = 43.590607
            long = -84.775927
            break
        case "Dow":
            lat = 43.586653
            long = -84.774755
            break
        case "Emmons":
            lat = 43.584078
            long = -84.771599
            break
        case "Finch":
            lat = 43.590714
            long = -84.773252
            break
        case "Foust":
            lat = 43.589231
            long = -84.770651
            break
        case "Grawn":
            lat = 43.592890
            long = -84.775919
            break
        case "Health Professions":
            lat = 43.590643
            long = -84.770522
            break
        case "Herrig":
            lat = 43.582871
            long = -84.771987
            break
        case "Indoor Athletic Complex":
            lat = 43.578664
            long = -84.772931
            break
        case "Kelly Shorts Stadium":
            lat = 43.577589
            long = -84.770871
            break
        case "Larzelere":
            lat = 43.593220
            long = -84.778423
            break
        case "Merrill":
            lat = 43.584357
            long = -84.776615
            break
        case "Moore":
            lat = 43.586707
            long = -84.773131
            break
        case "Music Building":
            lat = 43.588147
            long = -84.772647
            break
        case "Library":
            lat = 43.589117
            long = -84.774336
            break
        case "Pearce":
            lat = 43.586615
            long = -84.776076
            break
        case "Powers":
            lat = 43.591041
            long = -84.776783
            break
        case "Robinson":
            lat = 43.591883
            long = -84.778017
            break
        case "Ronan":
            lat = 43.592139
            long = -84.777067
            break
        case "Rose":
            lat = 43.580850
            long = -84.774053
            break
        case "Rowe":
            lat = 43.592928
            long = -84.769686
            break
        case "Saxe":
            lat = 43.583457
            long = -84.771585
            break
        case "Sloan":
            lat = 43.592221
            long = -84.773505
            break
        case "Smith":
            lat = 43.593033
            long = -84.774707
            break
        case "Student Activity Center":
            lat = 43.580432
            long = -84.774031
            break
        case "Sweeney":
            lat = 43.584289
            long = -84.774889
            break
        case "Thorpe":
            lat = 43.582963
            long = -84.774774
            break
        case "Trout":
            lat = 43.593000
            long = -84.779253
            break
        case "Troutman":
            lat = 43.584334
            long = -84.780021
            break
        case "Warriner":
            lat = 43.591226
            long = -84.775246
            break
        case "Wheeler":
            lat = 43.584356
            long = -84.778951
            break
        case "Wightman":
            lat = 43.590754
            long = -84.778188
            break
        case "Woldt":
            lat = 43.584766
            long = -84.772048
            break
        case "IET":
            lat = 43.585751
            long = -84.771770
            break
            
        default: break
        }
        
        let loc = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(lat),longitude: CLLocationDegrees(long), zoom: 16)
        gMapViews.camera = loc
        shouldCenter = false
        return lat.description + "," + long.description
    }
    
    
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        //The map was moved due to a user touching the map
        if(gesture){
            shouldCenter = false
        }else{
            shouldCenter = true
        }
    }
    
    @IBAction func goHomeConfirm(_ sender: Any) {
        blurEffectView.isHidden = true
        
        if(isKeyPresentInUserDefaults(key: "goingHome")){
            print("1")

            if(UserDefaults.standard.integer(forKey: "goingHome") == 1){
                closeHomePopup(self)
                UserDefaults.standard.set(0, forKey: "goingHome")
                lsAlert.backgroundColor = #colorLiteral(red: 1, green: 0, blue: 0.04472958599, alpha: 1)
                lsAlert.text = "Your location services are not on! Cannot pinpoint your location."
                lsAlert.isHidden = true
            }else{
                print("2")
                closeHomePopup(self)
                UserDefaults.standard.set(1, forKey: "goingHome")
                lsAlert.backgroundColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
                lsAlert.text = "Traveling to destination."
                lsAlert.isHidden = false
            }
        }else{
            print("2")
            closeHomePopup(self)
            UserDefaults.standard.set(1, forKey: "goingHome")
            lsAlert.backgroundColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
            lsAlert.text = "Traveling to destination."
            lsAlert.isHidden = false
        }
    }
    
    @IBAction func closeHomePopup(_ sender: Any) {
        goHomePopup.alpha = 0
        self.blurEffectView.isHidden = true
    }
    
    @IBAction func editGoHomeContact(_ sender: Any) {
        self.performSegue(withIdentifier: "goHomeContact", sender: self)
    }
    
    @IBOutlet var travelHereButton: UIButton!
    
    @IBAction func showDestinationPopup(){
        
        if(isKeyPresentInUserDefaults(key: "goingHome")){
            if(UserDefaults.standard.integer(forKey: "goingHome") == 1){
                travelHereButton.setTitle("Cancel Route", for: .normal)
                travelHereButton.backgroundColor = #colorLiteral(red: 1, green: 0, blue: 0.04472958599, alpha: 1)
            }else{
                travelHereButton.setTitle("Travel Here", for: .normal)
                travelHereButton.backgroundColor = #colorLiteral(red: 0.2859999794, green: 0.7938287467, blue: 0, alpha: 1)
            }
        }else{
            
        }
        
        if(goHomePopup.alpha == 0){
            
            goHomePopup.center = self.view.center

            self.blurEffectView.isHidden = false
            self.view.bringSubview(toFront: goHomePopup)
            goHomePopup.alpha = 1
        }else{
            closeHomePopup(self)
        }
    }
    
    func isKeyPresentInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
    
    //================================================================================
    //# MARK: - Location Manager
    //================================================================================
    
    func initializeLocationManager(){
        locationManager = CLLocationManager()
        
        if(CLLocationManager.authorizationStatus() != .authorizedWhenInUse || CLLocationManager.authorizationStatus() != .authorizedAlways){
            locationManager.requestAlwaysAuthorization()
        }
        
        if (CLLocationManager.locationServicesEnabled())
        {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.activityType = .fitness
            locationManager.distanceFilter = 10
            locationManager.startUpdatingLocation()
            gMapViews.isMyLocationEnabled = true
            gMapViews.settings.myLocationButton = true
            
        }
        
        if(CLLocationManager.authorizationStatus() == .authorizedWhenInUse || CLLocationManager.authorizationStatus() == .authorizedAlways){
            if(locationManager.location != nil){
                
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, monitoringDidFailFor region: CLRegion?, withError error: Error) {
        print("Monitoring failed for region with identifier: \(region!.identifier)")
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {print("Location Manager failed with the following error: \(error)")}
    
    func startMonitoring(geotification: Geotification) {
        if !CLLocationManager.isMonitoringAvailable(for: CLCircularRegion.self) {
            return
        }
        
        if CLLocationManager.authorizationStatus() != .authorizedAlways {}
        let region = self.region(withGeotification: geotification)
        locationManager.startMonitoring(for: region)
    }
    
    func addGeotification(coordinate: CLLocationCoordinate2D, radius: Double, identifier: String, note: String, eventType: EventType) {
        let clampedRadius = min(radius, locationManager.maximumRegionMonitoringDistance)
        let geotification = Geotification(coordinate: coordinate, radius: clampedRadius, identifier: identifier, note: note, eventType: eventType)
        homeGeo = geotification
        addRadiusOverlay(forGeotification: homeGeo)
        startMonitoring(geotification: geotification)
    }
    
    func stopMonitoring(geotification: Geotification) {
        for region in locationManager.monitoredRegions {
            guard let circularRegion = region as? CLCircularRegion, circularRegion.identifier == geotification.identifier else { continue }
            locationManager.stopMonitoring(for: circularRegion)
        }
    }
    
    func region(withGeotification geotification: Geotification) -> CLCircularRegion {
        let region = CLCircularRegion(center: geotification.coordinate, radius: geotification.radius, identifier: geotification.identifier)
        region.notifyOnEntry = (geotification.eventType == .onEntry)
        region.notifyOnExit = !region.notifyOnEntry
        return region
    }
    
    func addRadiusOverlay(forGeotification geotification: Geotification) {
        circ?.map = nil
        circ = GMSCircle(position: geotification.coordinate, radius: geotification.radius)
        circ?.fillColor = #colorLiteral(red: 0.1411764771, green: 0.3960784376, blue: 0.5647059083, alpha: 0.2615270322)
        circ?.strokeColor = #colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1)
        circ?.strokeWidth = 2
        circ?.map = gMapViews
    }
    
}

