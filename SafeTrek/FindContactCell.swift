//
//  FindContactCell.swift
//  SafeTrek
//
//  Created by Bryan Caragay on 10/9/16.
//  Copyright © 2016 Bryan Caragay. All rights reserved.
//

import UIKit

class FindContactCell: UITableViewCell {

    @IBOutlet var markerImage: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
