//
//  TermsAndConditions.swift
//  SafeTrek
//
//  Created by Bryan Caragay on 10/10/16.
//  Copyright © 2016 Bryan Caragay. All rights reserved.
//

import UIKit

class TermsAndConditions: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func accept(_ sender: AnyObject) {
        let alertController = UIAlertController(title: "Agree to Terms", message: "Do you accept the terms and conditions?", preferredStyle: UIAlertControllerStyle.alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "Accept", style: UIAlertActionStyle.default) {
            UIAlertAction in
            self.performSegue(withIdentifier: "accept", sender: self)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
}
