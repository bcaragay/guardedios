//
//  VC_SignUp_2.swift
//  SafeTrek
//
//  Created by Bryan Caragay on 2/18/16.
//  Copyright © 2016 Bryan Caragay. All rights reserved.
//

import UIKit
import Firebase
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class VC_SignUp_2: UIViewController, UITextFieldDelegate{

    @IBOutlet var fullName: UITextField!
    let ref = Database.database().reference()
    let activityIndictor: UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)

    @IBOutlet var gender: UISegmentedControl!
    var username : String = ""
    var password : String = ""
    var PIN : String = ""
    var lastPn : String = ""
    var org : String = ""
    
    @IBOutlet var addressField: UITextField!
    @IBOutlet var phoneNumberField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fullName.delegate = self
        
        activityIndictor.center = view.center
        self.view.addSubview(activityIndictor)
        activityIndictor.isHidden = true
        
        gender.selectedSegmentIndex = 0
    }
    
    func textFieldDidChange(_ textField: UITextField) {
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func finsh(){
        activityIndictor.isHidden = false
        activityIndictor.startAnimating()
        
        if(fullName.text != "" && phoneNumberField.text != ""){
            let newUser = [
                "full_name" : fullName.text!,
                "PIN" : PIN,
                "phone_number" : phoneNumberField.text!,
            ]

            Auth.auth().createUser(withEmail: username, password: password) {(user, error) in
                if error != nil {
                    self.activityIndictor.isHidden = true
                     self.activityIndictor.stopAnimating()
                
                    if let errCode = AuthErrorCode(rawValue: error!._code) {
                        let alert = SCLAlertView()

                        switch errCode {
                        case .invalidEmail:
                            alert.showError("Error In Email", subTitle: "This email is invalid!")
                        case .emailAlreadyInUse:
                            alert.showError("Error In Email", subTitle: "This email is already in use!")
                        default:
                            print("Create User Error: \(error!)")
                        }
                    }
                    print(error!)
                } else {
                     self.activityIndictor.isHidden = true
                    self.activityIndictor.stopAnimating()
                    let uid = user?.uid

                    if let user = user {
                        let changeRequest = user.createProfileChangeRequest()
                        
                        changeRequest.displayName = self.fullName.text

                        changeRequest.commitChanges { error in
                            if let error = error {
                                print(error)
                            }
                        }
                    }
                    
//                    FIRAuth.auth()?.currentUser?.sendEmailVerification(completion: { (error) in
//                        print("Email sent")
//                    })
                    
                    
                    _ = KeychainWrapper.setString(self.password, forKey: "password")
                    _ = KeychainWrapper.setString(self.username, forKey: "username")
                    let usersRef = self.ref.child("users")
                    usersRef.child(self.org).child(uid!).setValue(newUser)
                    
                    let appearance = SCLAlertView.SCLAppearance(
                        showCloseButton: false
                    )
                    
                    let alert = SCLAlertView(appearance: appearance)
                    
                    alert.addButton("Okay", action: {
                        //Logging the gender depending on what the user picked.
                        if(self.gender.selectedSegmentIndex == 0){
                            print("0")
                            self.ref.child("DataCollection").child(self.org).child("ios").observeSingleEvent(of: .value, with: { (snapshot) in
                                if(snapshot.exists()){
                                    print("1")
                                    var currentUses = Int(snapshot.value as! Int)
                                    currentUses += 1
                                    
                                    let newUses = ["ios" : currentUses]
                                    self.ref.child("DataCollection").child(self.org).updateChildValues(newUses)
                                    print("2")
                                    self.ref.child("DataCollection").child(self.org).child("Females").observeSingleEvent(of: .value, with: { (snapshot) in
                                        var currentUses = Int(snapshot.value as! Int)
                                        print("3")
                                        currentUses += 1
                                        
                                        let newUses = ["Females" : currentUses]
                                        self.ref.child("DataCollection").child(self.org).updateChildValues(newUses)
                                        
                                })
                                }
                            })
                        }else if(self.gender.selectedSegmentIndex == 1){
                            self.ref.child("DataCollection").child(self.org).child("ios").observeSingleEvent(of: .value, with: { (snapshot) in
                                if(snapshot.exists()){
                                    var currentUses = Int(snapshot.value as! Int)
                                    currentUses += 1
                                    
                                    let newUses = ["ios" : currentUses]
                                    self.ref.child("DataCollection").child(self.org).updateChildValues(newUses)
                                    
                                    self.ref.child("DataCollection").child(self.org).child("Males").observeSingleEvent(of: .value, with: { (snapshot) in
                                        var currentUses = Int(snapshot.value as! Int)
                                        currentUses += 1
                                        
                                        let newUses = ["Males" : currentUses]
                                        self.ref.child("DataCollection").child(self.org).updateChildValues(newUses)
                                        
                                    })
                                }
                            })
                        }else{
                            //Not Saying their gender
                        }
                        
                        self.activityIndictor.isHidden = true
                        self.activityIndictor.stopAnimating()
                        
                        let ref = Database.database().reference()
                        
                        let refValid = ref.child("ContractStatus").child(self.org.lowercased())
                        
                        ref.child("ContractStatus").observeSingleEvent(of: .value, with: {snapshot in
                            
                            //Make sure the organization exists, if not, they haven't talked with us yet
                            if(snapshot.hasChild(self.org.lowercased())){
                                refValid.observeSingleEvent(of: .value, with: { snapshot in
                                    if(snapshot.value as! String == "open" || snapshot.value as! String == "contract"){
                                        //Open login is active
                                        self.performSegue(withIdentifier: "tutorial", sender: self)
                                    }else{
                                        self.performSegue(withIdentifier: "payment", sender: self)

                                    }
                                })
                            }
                        })
                    })
                    
                    self.activityIndictor.isHidden = true
                    self.activityIndictor.stopAnimating()
                    
                    alert.showSuccess("Congrats", subTitle: "You are now signed up.")
                }
            }
        }else{
            activityIndictor.isHidden = true
            activityIndictor.stopAnimating()
            
            let alert = SCLAlertView()
            alert.showError("Uh Oh!", subTitle: "All fields must be filled in.")

        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if(segue.identifier == "payment"){
//            let vc : CollectPaymentViewController = segue.destination as! CollectPaymentViewController
//            vc.creatingAccount = true
//            vc.org = self.org
//        }
    }
    
    
    
    @IBAction func pnEditor(_ sender: AnyObject) {
        if(lastPn > phoneNumberField.text){
            if(phoneNumberField.text?.count == 4){
                phoneNumberField.text?.remove(at: (phoneNumberField.text?.index((phoneNumberField.text?.startIndex)!, offsetBy: 3))!)
            }
            if(phoneNumberField.text?.count == 8){
                phoneNumberField.text?.remove(at: (phoneNumberField.text?.index((phoneNumberField.text?.startIndex)!, offsetBy: 7))!)
            }
        }else{
            if(phoneNumberField.text?.count == 4){
                phoneNumberField.text?.insert("-", at: (phoneNumberField.text?.index((phoneNumberField.text?.startIndex)!, offsetBy: 3))!)
            }
            if(phoneNumberField.text?.count == 8){
                phoneNumberField.text?.insert("-", at: (phoneNumberField.text?.index((phoneNumberField.text?.startIndex)!, offsetBy: 7))!)
            }
        }
        lastPn = phoneNumberField.text!
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
