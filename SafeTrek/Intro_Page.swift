//
//  Intro_Page.swift
//  SafeTrek
//
//  Created by Bryan Caragay on 3/24/16.
//  Copyright © 2016 Bryan Caragay. All rights reserved.
//

import UIKit

class Intro_Page: UIPageViewController, UIPageViewControllerDataSource {

    fileprivate(set) lazy var orderedViewControllers: [UIViewController] = {
        return[self.newVC("one"),
               self.newVC("two"),
               self.newVC("three"),
               self.newVC("four"),
               self.newVC("five"),
               self.newVC("six"),
               self.newVC("seven"),]
    }()
    
    fileprivate func newVC(_ color: String) -> UIViewController{
        return UIStoryboard(name: "Main", bundle: nil) .
            instantiateViewController(withIdentifier: "\(color)")
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        
        let subViews: NSArray = view.subviews as NSArray
        var scrollView: UIScrollView? = nil
        var pageControl: UIPageControl? = nil
        
        for view in subViews {
            if (view as AnyObject).isKind(of: UIScrollView.self){
                scrollView = view as? UIScrollView
            }else if (view as AnyObject).isKind(of: UIPageControl.self){
                pageControl = view as? UIPageControl
            }
        }
        
        if(scrollView != nil && pageControl != nil){
            scrollView?.frame = view.bounds
            view.bringSubview(toFront: pageControl!)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        dataSource = self
        self.view.backgroundColor = UIColor.clear
        
        if let firstVC = orderedViewControllers.first{
            setViewControllers([firstVC], direction: .forward, animated: true, completion: nil)
        }
        UIPageControl.appearance().pageIndicatorTintColor           = UIColor.lightGray
        UIPageControl.appearance().currentPageIndicatorTintColor    = UIColor.init(red: 82, green: 198, blue: 240, alpha: 1)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else{
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else{
            return nil
        }
        
        return orderedViewControllers[previousIndex]
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return orderedViewControllers.count
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        guard let firstViewController = viewControllers?.first,
            let firstViewControllerIndex = orderedViewControllers.index(of: firstViewController) else {
                return 0
        }
        
        return firstViewControllerIndex
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else{
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = orderedViewControllers.count
        
        guard orderedViewControllersCount != nextIndex else{
            return nil
        }
        
        guard orderedViewControllersCount > nextIndex else{
            return nil
        }
        
        return orderedViewControllers[nextIndex]
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
