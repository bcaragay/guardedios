//
//  SchoolTableViewCell.swift
//  SafeTrek
//
//  Created by Bryan Caragay on 8/3/17.
//  Copyright © 2017 Bryan Caragay. All rights reserved.
//

import UIKit

class SchoolTableViewCell: UITableViewCell {

    @IBOutlet var schoolTitleLabel: UILabel!
    @IBOutlet var schoolDescriptionLabel: UILabel!
    @IBOutlet var bgView: UIView!
    @IBOutlet var checked: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
