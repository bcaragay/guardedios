//
//  VC_Call.swift
//  SafeTrek
//
//  Created by Bryan Caragay on 2/19/16.
//  Copyright © 2016 Bryan Caragay. All rights reserved.
//

import UIKit
import AudioToolbox
import AVFoundation
class VC_Call: UIViewController {
    
    @IBOutlet var callStatus: UIImageView!
    
    @IBOutlet var info: UILabel!
    @IBOutlet var HangUpButton: UIButton!
    @IBOutlet var AcceptCallButton: UIButton!
    @IBOutlet var DeclineCallButton: UIButton!
    var mySound: SystemSoundID = 0
    var audioPlayer = AVAudioPlayer()
    var name = ""
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true
        HangUpButton.isEnabled = false
        
        let session = AVAudioSession.sharedInstance()
        try! session.setCategory(AVAudioSessionCategoryPlayback)
        try! session.setActive(true)
        
        let sound = URL(fileURLWithPath: Bundle.main.path(forResource: "iPhone Ringtone Opening", ofType: "mp3")!)
        do { audioPlayer = try AVAudioPlayer(contentsOf: sound, fileTypeHint: nil) }
        catch let error as NSError { print(error.description) }
        
        audioPlayer.prepareToPlay()
        audioPlayer.play()
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func AcceptButton(_ sender: AnyObject) {
        info.text = "Now hang up when you are ready!"
        callStatus.image = UIImage(named: "CurrentCall")
        AcceptCallButton.isEnabled = false
        DeclineCallButton.isEnabled = false
        audioPlayer.stop()
        HangUpButton.isEnabled = true
    }
    
}
