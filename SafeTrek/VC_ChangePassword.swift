//
//  VC_ChangePassword.swift
//  SafeTrek
//
//  Created by Bryan Caragay on 4/7/16.
//  Copyright © 2016 Bryan Caragay. All rights reserved.
//

import UIKit
import Firebase

class VC_ChangePassword: UIViewController,UITextFieldDelegate {

    @IBOutlet var newPassRepeat: UITextField!
    @IBOutlet var newPass: UITextField!
    @IBOutlet var oldPass: UITextField!
    @IBOutlet var email: UITextField!
    var org : String = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func closeButton(){
        _ = navigationController?.popViewController(animated: true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func change(_ sender: AnyObject) {
        
        if(newPass.text == newPassRepeat.text){
            
            let user = Auth.auth().currentUser
            
            user?.updatePassword(to: newPass.text!){ error in
                if error != nil{
                    let alertView = SCLAlertView()
                    alertView.showError("Oops!", subTitle: "Some info is not correct!")
                }else{
                    
                    //Set up popup notification
                    let appearance = SCLAlertView.SCLAppearance(
                        showCloseButton: false // hide default button
                    )
                    let alertView = SCLAlertView(appearance: appearance)
                    alertView.addButton("Okay", target:self, selector:#selector(VC_ChangePassword.closeButton))
                    alertView.showSuccess("Yay!", subTitle: "Your password has been changed." )
                }
            }
        }else{
            let alert = UIAlertController(title: "Oops!", message:"Your passwords do not match.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default) { _ in })
            self.present(alert, animated: true){}
        }
    }
}
