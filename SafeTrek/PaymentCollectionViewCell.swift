//
//  PaymentCollectionViewCell.swift
//  SafeTrek
//
//  Created by Bryan Caragay on 7/15/17.
//  Copyright © 2017 Bryan Caragay. All rights reserved.
//

import UIKit

class PaymentCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var card: UIView!
    @IBOutlet var title: UILabel!
    @IBOutlet var cost: UILabel!
    @IBOutlet var bottomView: UIView!
    @IBOutlet var topView: UIView!
    @IBOutlet var selectButton: UIButton!
    @IBOutlet var descriptionLabel: UILabel!
}
