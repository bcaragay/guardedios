//
//  VC_Button.swift
//  SafeTrek
//
//  Created by Bryan Caragay on 2/19/16.
//  Copyright © 2016 Bryan Caragay. All rights reserved.
//

import UIKit

class VC_Button: UIViewController {

    @IBOutlet var aButton: UIButton!
    @IBOutlet var inst: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        aButton.addTarget(self, action: #selector(VC_Button.HoldDown(_:)), for: UIControlEvents.touchDown)
        aButton.addTarget(self, action: #selector(VC_Button.holdRelease(_:)), for: UIControlEvents.touchUpInside);
        aButton.addTarget(self, action: #selector(VC_Button.holdRelease(_:)), for: UIControlEvents.touchUpOutside)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func HoldDown(_ sender:UIButton)
    {
        inst.text = "Now release the button when you feel safe!"
    }
    
    @objc func holdRelease(_ sender:UIButton)
    {
        self.performSegue(withIdentifier: "code", sender: self)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
