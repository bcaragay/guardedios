//
//  VC_Sign_In.swift
//  SafeTrek
//
//  Created by Bryan Caragay on 2/16/16.
//  Copyright © 2016 Bryan Caragay. All rights reserved.
//

import UIKit
import Firebase
import LocalAuthentication
import CoreLocation

class VC_Sign_In: UIViewController,UITextFieldDelegate {

    @IBOutlet var b_signUp: UIButton!
    @IBOutlet var b_forgotPassword: UIButton!
    @IBOutlet var passwordField: UITextField!
    @IBOutlet var usernameField: UITextField!
    @IBOutlet var version: UILabel!
    @IBOutlet var loader: UIActivityIndicatorView!
    
    var locationManager: CLLocationManager!
    var uid = "", org = "", id = "", versionNumber = ""
    
    var exempt = false
    var loaded = false
    var recordData = true
    var validSubscription = false
    
    override func viewWillAppear(_ animated: Bool) {
        let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        
        if(loader.isAnimating){
            self.stopLoading()
        }
        
        if let build = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
            self.version.text = version! + "." + build
        }

        locationManager = CLLocationManager()

        //There is a user already signed in
        if (Auth.auth().currentUser) != nil {
            self.startLoading()
            if(!self.loaded){
                checkUserVerified()
            }
        } else {
            //No user is signed in, Fingerprint scanner
            if(CLLocationManager.authorizationStatus() == .authorizedWhenInUse || CLLocationManager.authorizationStatus() == .authorizedAlways){
                let context = LAContext()
                var error: NSError?
                
                if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
                    if(KeychainWrapper.stringForKey("username") != nil && KeychainWrapper.stringForKey("password") != nil){
                        let reason = "Log in to Guarded using your fingerprint ID"
                        
                        context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason) {
                            [unowned self] success, authenticationError in
                            self.startLoading()

                            DispatchQueue.main.async {
                                if success {
                                //Fingerprint worked!
                                    Auth.auth().signIn(withEmail: KeychainWrapper.stringForKey("username")!, password: KeychainWrapper.stringForKey("password")!) { (user, error) in
                                        if error != nil {
                                            self.stopLoading()

                                            //Fingerprint scanner was unable to log user in
                                            let alert = UIAlertController(title: "Oops!", message:"Something went wrong, please try to log in again!", preferredStyle: .alert)
                                            alert.addAction(UIAlertAction(title: "OK", style: .default) { _ in })
                                            self.present(alert, animated: true){}
                                        } else {
                                            //Fingerprint scanner logged in user!
                                            self.checkUserVerified()
                                        }
                                    }
                                } else {
                                    //Fingerprint didn't work, or was canceled
                                    self.stopLoading()
                                }
                            }
                        }
                    }
                } else {
                    self.stopLoading()
                    
                    let ac = UIAlertController(title: "Touch ID not available", message: "Touch ID is not enabled on your device.", preferredStyle: .alert)
                    ac.addAction(UIAlertAction(title: "OK", style: .default))
                    present(ac, animated: true)
                }
            } else{
                locationManager.requestAlwaysAuthorization()
            }
        }
        
        if(CLLocationManager.authorizationStatus() != .authorizedWhenInUse || CLLocationManager.authorizationStatus() != .authorizedAlways){
            locationManager.requestAlwaysAuthorization()
        }

        self.navigationController!.navigationBar.barTintColor = UIColor(red: 0.32, green: 0.78, blue: 0.94, alpha: 1.0)
        navigationController?.navigationBar.tintColor = UIColor.white
        navigationController!.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let user = Auth.auth().currentUser{
            uid = user.uid
        }
        
        passwordField.delegate = self
        usernameField.delegate = self
        
        passwordField.addTarget(self, action: #selector(VC_Sign_In.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        usernameField.addTarget(self, action: #selector(VC_Sign_In.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
    }
    
    
    //Get the email extension (cmich, emich, etc)
    func checkEmailExtention(s: String) -> String {
        var v = s.components(separatedBy: "@").last?.components(separatedBy: ".")

        let id = s.components(separatedBy: "@").first
        self.id = id!
        v?.removeLast()
        print("Email Extension: " + (v!.last)!)
        
        
        return (v!.last)!
    }
    
    @IBAction func scanFingerprint(_ sender: AnyObject) {
        let context = LAContext()
        var error: NSError?
        self.startLoading()

        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            //Check to see if the user has created an account before.
            if(KeychainWrapper.stringForKey("username") != nil && KeychainWrapper.stringForKey("password") != nil){
                let reason = "Log in to Guarded using your fingerprint ID"
                
                context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason) {
                    [unowned self] success, authenticationError in
                    print("2")
                    DispatchQueue.main.async {
                        if success {
                            print("3")
                            //Fingerprint worked!
                            Auth.auth().signIn(withEmail: KeychainWrapper.stringForKey("username")!, password: KeychainWrapper.stringForKey("password")!) { (user, error) in
                                if error != nil {
                                    self.stopLoading()

                                    //Fingerprint scanner was unable to log user in
                                    let alert = UIAlertController(title: "Oops!", message:"Something went wrong, please try to log in again!", preferredStyle: .alert)
                                    alert.addAction(UIAlertAction(title: "OK", style: .default) { _ in })
                                    self.present(alert, animated: true){}
                                } else {
                                    print("4")
                                    //Fingerprint scanner logged in user!
                                    self.checkUserVerified()
                                }
                            }
                        } else {
                            print("5")
                            self.stopLoading()
                            //Fingerprint didn't work, or was canceled
                        }
                    }
                }
            }else{
                //Never logged in before
                self.stopLoading()
                //Fingerprint scanner was unable to log user in
                let alert = UIAlertController(title: "Oops!", message:"You can only use the fingerprint scanner after you have logged in at least once!", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default) { _ in })
                self.present(alert, animated: true){}
            }
        }else{
            print("-1")
            self.stopLoading()
        }
    }
    
    //Set the organization of the user in the main page
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "login"){
            self.stopLoading()
            
            let vc : VC_Main_Page = segue.destination as! VC_Main_Page
            vc.org = self.org
            
            //Don't record data when using the dummy login
            vc.recordData = recordData

            Messaging.messaging().subscribe(toTopic: "emich")
            print("Subscribed to emich")
        }
    }
    
    
    // MARK: Text Field Handlers

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        textField.backgroundColor = UIColor.white
    }
    

    // MARK: Logging In and Verification

    @IBAction func logIn(_ sender: AnyObject) {
        self.startLoading()
        
        if(usernameField.text != "" && passwordField.text != ""){
            if(usernameField.text == "dummy@dummy.com" && passwordField.text == "dummypassword"){
                self.stopLoading()
                self.performSegue(withIdentifier: "login", sender: self)
                recordData = false
                self.loaded = true
            }
            
            Auth.auth().signIn(withEmail: usernameField.text!, password: passwordField.text!) { (user, error) in

                if error != nil {
                    self.stopLoading()
                    
                    SCLAlertView().showError("Oops!", subTitle: "Please check your credentials and try again!", closeButtonTitle: "Okay")
                } else {
                    _ = KeychainWrapper.setString(self.passwordField.text!, forKey: "password")
                    _ = KeychainWrapper.setString(self.usernameField.text!, forKey: "username")
    
                    self.checkUserVerified()
                }
            }
        }else{
            loader.isHidden = true
            self.loader.stopAnimating()
            
            if(usernameField.text == ""){
                usernameField.backgroundColor = UIColor(red: 0.95, green: 0.77, blue: 0.77, alpha: 1.0)
            }
            
            if(passwordField.text == ""){
                passwordField.backgroundColor = UIColor(red: 0.95, green: 0.77, blue: 0.77, alpha: 1.0)
            }
        }
    }
    
    func sendEmailVerification(user: User){
        user.sendEmailVerification(completion: {(error) in})
    }
    
    func checkUserVerified(){
        let user = Auth.auth().currentUser
        
        if(user?.isEmailVerified)!{
            let ref = Database.database().reference()
            self.org = self.checkEmailExtention(s: (Auth.auth().currentUser?.email)!)

            let refValid = ref.child("ContractStatus").child(self.org.lowercased())

            ref.child("ContractStatus").observeSingleEvent(of: .value, with: {snapshot in
                //Make sure the organization exists, if not, they haven't talked with us yet
                if(snapshot.hasChild(self.org.lowercased())){
                    refValid.observeSingleEvent(of: .value, with: { snapshot in
                        if(snapshot.value as! String == "individual" || snapshot.value as! String == "contract" || snapshot.value as! String == "open"){
                            //The user's school paid for their contracts, no need for a subscription
                            if(snapshot.value as! String == "contract"){
                                self.stopLoading()
                                self.performSegue(withIdentifier: "login", sender: self)
                                self.loaded = true
                            }else if(snapshot.value as! String == "open"){
                                //Used to give all students free access for a period of time
                                print("Open sign in is active.")
                                self.stopLoading()
                                self.performSegue(withIdentifier: "login", sender: self)
                                self.loaded = true
                            }else{
                                //If the organization has an individual payment status, check who is exempt from paying
                                ref.child("ExceptionAccounts").observeSingleEvent(of: .value, with: {snapshot in
                                    if(snapshot.hasChild(self.id)){
                                        print("User is exempt")
                                        self.stopLoading()
                                        self.performSegue(withIdentifier: "login", sender: self)
                                        self.loaded = true
                                    }else{
                                        if(self.checkSubscription()){
                                            print("User has a subscription")
                                            self.stopLoading()
                                            self.performSegue(withIdentifier: "login", sender: self)
                                            self.loaded = true
                                        }else{
                                            if(!self.loaded){
                                                //User has no valid subscription
                                                self.performSegue(withIdentifier: "paymentPlan", sender: self)
                                                self.loaded = true
                                            }
                                        }
                                    }
                                })
                            }
                        }else{
                            //If the organization has an invalid status
                            self.stopLoading()
                            let alert = SCLAlertView()
                            
                            alert.showError("Oops!",
                                            subTitle: "We're not at your school yet, but we're coming soon!",
                                            closeButtonTitle: "Okay")
                        }
                    })
                }else{
                    //If the organization doesn't even have a valid/invalid status - they have never had a contract with us before.
                    let alert = SCLAlertView()
                    
                    self.stopLoading()
                    
                    alert.showError("Oops!",
                                    subTitle: "We're not at your school yet, but we're coming soon!",
                                    closeButtonTitle: "Okay")
                }
            })
        }else{
            self.stopLoading()
            
            let alert = UIAlertController(title: "Verify Email",
                                          message:"Your email has not been verified yet!",
                                          preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Verify", style: .default) { _ in
                
                //Send the verification email if the user chooses.
                self.sendEmailVerification(user: user!)
                
                let alert1 = UIAlertController(title: "Email Sent",
                                               message:"A verification has been sent to your email.",
                                               preferredStyle: .alert)
                
                alert1.addAction(UIAlertAction(title: "Okay", style: .default) { _ in})
                self.present(alert1, animated: true){}
            })
            
            alert.addAction(UIAlertAction(title: "Close", style: .default) { _ in})
            
            self.present(alert, animated: true){}
        }
    }
    
    func checkSubscription() -> Bool{
        var valid = false
        let IDs = ["com.guarded.sub.monthly", "com.guarded.sub.yearly", "com.guarded.sub.lifetime"]
        
        for id in IDs{
            if(UserDefaults.standard.bool(forKey: id)){
                validSubscription = true
                print("user has a valid subscription")
                valid = true
            }else{
                print("user has no valid subscription")
            }
        }
        
        return valid
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    func startLoading(){
        self.loader.isHidden = false
        self.loader.startAnimating()
    }
    
    func stopLoading(){
        self.loader.isHidden = true
        self.loader.stopAnimating()
    }
}


