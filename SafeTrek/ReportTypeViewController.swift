

import UIKit

class ReportTypeViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet var collectionView: UICollectionView!
    
    var incidentTypes = ["Theft", "Assault", "Burglary", "Car Accident", "Sexual Assault", "Arson"]
    
    var pageViewController: ReportViewController!
    var lastIndexPath: IndexPath!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.pageViewController = self.storyboard?.instantiateViewController(withIdentifier: "PageViewController") as! ReportViewController

    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "report", for: indexPath as IndexPath) as! ReportCollectionViewCell
        
        cell.label.text = incidentTypes[indexPath.row]
        
        // shadow
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOffset = CGSize(width: 3, height: 3)
        cell.layer.shadowOpacity = 0.3
        cell.layer.shadowRadius = 3.0

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(lastIndexPath != nil){
            let cell = collectionView.cellForItem(at: lastIndexPath) as! ReportCollectionViewCell
            cell.backgroundColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)
            cell.label.textColor = #colorLiteral(red: 0.3093238771, green: 0.3507784307, blue: 0.4080462158, alpha: 1)
        }
        
        lastIndexPath = indexPath
        
        collectionView.deselectItem(at: indexPath, animated: true)
        let cell = collectionView.cellForItem(at: indexPath) as! ReportCollectionViewCell
        cell.backgroundColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
        cell.label.textColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)
    }
    
    @IBAction func next(){
        pageViewController.goToNextPage()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return incidentTypes.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
}
