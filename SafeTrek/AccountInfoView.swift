//
//  AccountInfoView.swift
//  SafeTrek
//
//  Created by Bryan Caragay on 1/26/16.
//  Copyright © 2016 Bryan Caragay. All rights reserved.
//

import UIKit

class AccountInfoView: UIViewController {

    @IBOutlet var emailField: UITextField!
    @IBOutlet var usernameField: UITextField!
    @IBOutlet var passwordField: UITextField!
    @IBOutlet var repeastPasswordField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func Next(_ sender: AnyObject) {
        self.performSegue(withIdentifier: "pi", sender: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       
    }
}
