//
//  VC_Sign_Up.swift
//  SafeTrek
//
//  Created by Bryan Caragay on 2/5/16.
//  Copyright © 2016 Bryan Caragay. All rights reserved.
//

import UIKit

class VC_Sign_Up: UIViewController {

    @IBOutlet var imagePickerButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        imagePickerButton.imageView?.contentMode = UIViewContentMode.scaleAspectFill
        self.imagePickerButton.layer.borderWidth = 1
        self.imagePickerButton.layer.borderColor = UIColor(red:222/255.0, green:225/255.0, blue:227/255.0, alpha: 1.0).cgColor
        
        imagePickerButton.layer.masksToBounds = false
        imagePickerButton.layer.cornerRadius = imagePickerButton.frame.size.width/2
        imagePickerButton.clipsToBounds = true
    }

    @IBAction func PickImage(_ sender: AnyObject) {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
