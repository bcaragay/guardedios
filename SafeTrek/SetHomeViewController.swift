//
//  SetHomeViewController.swift
//  SafeTrek
//
//  Created by Bryan Caragay on 6/8/17.
//  Copyright © 2017 Bryan Caragay. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class SetHomeViewController: UIViewController, GMSMapViewDelegate, UITextFieldDelegate {

    @IBOutlet var gMapView: GMSMapView!
    @IBOutlet var addressField: TextFieldSpacer!
    var circle = GMSCircle()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addressField.returnKeyType = UIReturnKeyType.search
        gMapView.mapType = GMSMapViewType.satellite

        addressField.delegate = self
        setupMap()
    }

    func setupMap(){
        gMapView.camera = GMSCameraPosition(target:CLLocationCoordinate2DMake(42.877742, -97.380979), zoom: 3, bearing: 0, viewingAngle: 0)
    }

    @IBAction func exitViewController(_ sender: Any) {
            dismiss(animated: true, completion: nil)
    }
    
    @IBAction func search(_ sender: Any) {
        searchAddress()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        searchAddress()
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

    @IBAction func setDestination(_ sender: Any) {
        setupCircle(location: gMapView.camera.target)
    }

    func setupCircle(location: CLLocationCoordinate2D){
        UserDefaults.standard.set(location.latitude, forKey: "HomeLocationLat")
        UserDefaults.standard.set(location.longitude, forKey: "HomeLocationLong")

        circle.map = nil
        circle = GMSCircle(position: location, radius: 25)
        circle.strokeColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        circle.fillColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 0.5518716449)
        
        circle.map = gMapView
        exitViewController(self)
    }
    
    func searchAddress() {
        addressField.endEditing(true)

        let address = addressField.text
        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString(address!) { (placemarks, error) in
            if(error != nil){
            }else{
                if let placemarks = placemarks {
                    if placemarks.count != 0 {
                        //Get the address
                        let placemark = placemarks[0]
                        
                        let zoomOut = GMSCameraPosition(target: self.gMapView.camera.target, zoom: 2, bearing: 0, viewingAngle: 0)
                        
                        self.gMapView.animate(to: zoomOut)
                     
                        
                        CATransaction.begin()
                        CATransaction.setValue(0.5, forKey: kCATransactionAnimationDuration)
                        let loc = GMSCameraPosition(target:(placemark.location?.coordinate)!, zoom: 18, bearing: 0, viewingAngle: 0)
                        
                        
                        self.gMapView.animate(to: loc)
                        CATransaction.commit()
                    
                        
                        let addressDictionary = placemark.addressDictionary
                        
                        let address = addressDictionary!["Street"] as! String
                        let city = addressDictionary!["City"] as! String
                        let state = addressDictionary!["State"] as! String
                        let zip = addressDictionary!["ZIP"] as! String
                        
                        print("\(address) \(city) \(state) \(zip)")
                    }
                }
            }
        }
    }
}
