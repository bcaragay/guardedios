//
//  ViewController.swift
//  SafeTrek
//
//  Created by Bryan Caragay on 1/13/16.
//  Copyright © 2016 Bryan Caragay. All rights reserved.
//

import UIKit
import CoreLocation
import MessageUI
import AVFoundation

class ViewController: UIViewController, CLLocationManagerDelegate, MFMessageComposeViewControllerDelegate {

    var location : CLLocation!
    
    var bPressed : Bool = false;
    let locationManager = CLLocationManager()
    @IBOutlet var button: UIButton!
    var mySound: SystemSoundID = 0
    var audioPlayer = AVAudioPlayer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
     //   self.myUIImageView.userInteractionEnabled = true

        let upSwipe = UISwipeGestureRecognizer(target: self, action: #selector(ViewController.handleSwipe(_:)))
        let downSwipe = UISwipeGestureRecognizer(target: self, action: #selector(ViewController.handleSwipe(_:)))
        
        upSwipe.direction = .up
        downSwipe.direction = .down

        view.addGestureRecognizer(upSwipe)
        view.addGestureRecognizer(downSwipe)

        button.addTarget(self, action: #selector(ViewController.buttonDown(_:)), for: .touchDown)
        button.addTarget(self, action: #selector(ViewController.buttonUp(_:)), for: [.touchUpInside, .touchUpOutside])
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        let session = AVAudioSession.sharedInstance()
        try! session.setCategory(AVAudioSessionCategoryPlayback)
        try! session.setActive(true)
        
        let sound = URL(fileURLWithPath: Bundle.main.path(forResource: "Police Siren Sound Effect", ofType: "mp3")!)
        do { audioPlayer = try AVAudioPlayer(contentsOf: sound, fileTypeHint: nil) }
        catch let error as NSError { print(error.description) }
        
        audioPlayer.prepareToPlay()
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        switch result.rawValue {
            
        default:
            break
        }
        controller.dismiss(animated: true, completion: nil)
    }

    @IBAction func fakeCall(_ sender: AnyObject) {
        self.performSegue(withIdentifier: "call", sender: self)
    }
    
    @IBAction func alarm(_ sender: AnyObject) {
        flash()
    }
    
    override var canBecomeFirstResponder : Bool {
        return true
    }
    
    override func motionEnded(_ motion: UIEventSubtype, with event: UIEvent?) {
        sendMessage()
    }
    
    func sendMessage() {
        let messageVC = MFMessageComposeViewController()
        print(location.coordinate.longitude)

        messageVC.body = "Help! I am in trouble at \n\ngoogle.com/maps/?q=" + location.coordinate.latitude.description + "," + location.coordinate.longitude.description

        if((UserDefaults.standard.object(forKey: "emergencyNumbers")) != nil){
            messageVC.recipients = (UserDefaults.standard.object(forKey: "emergencyNumbers") as? [String])!
        }

        messageVC.messageComposeDelegate = self
        present(messageVC, animated: true, completion: nil)

    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        let userLocation:CLLocation = locations[0]
        location = userLocation
        
    }
    
    @objc func buttonDown(_ sender: AnyObject) {
        bPressed = true
    }

    @objc func buttonUp(_ sender: AnyObject) {
        bPressed = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func handleSwipe(_ sender:UISwipeGestureRecognizer){
        print("Sending alert...")

        if(sender.direction == .up && bPressed){
            print("Sending alert...")
            sendMessage()
            bPressed = false
        }else if(sender.direction == .down && bPressed){
            print("Canceling alert...")
            bPressed = false
        }
    }
    
    func flash(){
        
        audioPlayer.play()
        
        while(audioPlayer.isPlaying){
            let device = AVCaptureDevice.default(for: AVMediaType.video)
            if (device?.hasTorch)! {
                do {
                    try device?.lockForConfiguration()
                    if (device?.torchMode == AVCaptureDevice.TorchMode.on) {
                        device?.torchMode = AVCaptureDevice.TorchMode.off
                    } else {
                        try device?.setTorchModeOn(level: 1.0)
                    }
                    device?.unlockForConfiguration()
                } catch {
                    print(error)
                }
            }
            sleep(1)
        }
    }
}

