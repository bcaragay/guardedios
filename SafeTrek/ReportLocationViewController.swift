//
//  ReportLocationViewController.swift
//  SafeTrek
//
//  Created by Bryan Caragay on 10/12/17.
//  Copyright © 2017 Bryan Caragay. All rights reserved.
//

import UIKit

class ReportLocationViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {

    var locations = ["Health Professions", "Anspach","Music Building","Art Gallery", "Library","Barnes","Pearce","Beddow","Powers","Brooks","Robinson","Calkins","Ronan","Carey","Rose","Carlin Alumni House","Rowe","Cobb","Student Activity Center","Combined Services","Saxe","Dow","Sloan","Emmons","Smith","Finch","Sweeney","Foust","Thorpe","Grawn","Trout","Herrig","Troutman","Indoor Athletic Complex","University Center","IET","Warriner","Kelly Shorts Stadium","Wheeler","Larzelere","Wightman","Merrill","Woldt","Moore"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locations.sort(){$0 < $1}
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return locations.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "report", for: indexPath as IndexPath) as! ReportCollectionViewCell
        
        cell.label.text = locations[indexPath.row]
        
        // shadow
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOffset = CGSize(width: 3, height: 3)
        cell.layer.shadowOpacity = 0.3
        cell.layer.shadowRadius = 3.0

        return cell
    }
}
