//
//  ReportCollectionViewCell.swift
//  SafeTrek
//
//  Created by Bryan Caragay on 10/12/17.
//  Copyright © 2017 Bryan Caragay. All rights reserved.
//

import UIKit

class ReportCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var label: UILabel!
}
