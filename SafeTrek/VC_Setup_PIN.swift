//
//  VC_Setup_PIN.swift
//  SafeTrek
//
//  Created by Bryan Caragay on 3/2/16.
//  Copyright © 2016 Bryan Caragay. All rights reserved.
//

import UIKit
import Firebase
class VC_Setup_PIN: UIViewController, UIPopoverPresentationControllerDelegate {

    var num : String = ""
    var username : String = ""
    var password : String = ""
    
    var org : String = ""
    
    @IBOutlet var dotDisplay: UIImageView!
    var firstPin : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func clear(_ sender: AnyObject) {
        if(num.count > 0){
            num.remove(at: num.index(before: num.endIndex))
            dotDisplay.image = UIImage(named: "pin_" + (num.count).description)
        }
    }
    
    @IBAction func one(_ sender: AnyObject) {
        if(num.count < 4){
            num += "1"
            check()
        }
    }
    @IBAction func two(_ sender: AnyObject) {
        if(num.count < 4){
            num += "2"
            check()
        }
    }
    @IBAction func three(_ sender: AnyObject) {
        if(num.count < 4){
            num += "3"
            check()
        }
    }
    @IBAction func four(_ sender: AnyObject) {
        if(num.count < 4){
            num += "4"
            check()
        }
    }
    @IBAction func five(_ sender: AnyObject) {
        if(num.count < 4){
            num += "5"
            check()
        }
    }

    @IBAction func six(_ sender: AnyObject) {
        if(num.count < 4){
            num += "6"
            check()
        }
    }
    
    @IBAction func seven(_ sender: AnyObject) {
        if(num.count < 4){
            num += "7"
            check()
        }
    }
    @IBAction func eight(_ sender: AnyObject) {
        if(num.count < 4){
            num += "8"
            check()
        }
    }
    @IBAction func nine(_ sender: AnyObject) {
        if(num.count < 4){
            num += "9"
            check()
        }
    }
    @IBAction func zero(_ sender: AnyObject) {
        if(num.count < 4){
            num += "0"
            check()
        }
    }
    
    func check(){
        dotDisplay.image = UIImage(named: "pin_" + (num.count).description)
    }
    
    @IBAction func done(_ sender: AnyObject) {
        //Check if the PIN is 4 numbers long
        if(num.count == 4){
            if(firstPin == ""){
                firstPin = num
                num = ""
                dotDisplay.image = UIImage(named: "pin_" + (num.count).description)
                let alertView = SCLAlertView()
                
                alertView.showInfo("Nice!", subTitle: "Enter your PIN one more time to confirm!", closeButtonTitle: "Okay")
            }else{
                if(firstPin == num){
                    self.performSegue(withIdentifier: "sign_up_2", sender: self)
                }else{
                    let alertView = SCLAlertView()
                    
                    alertView.showError("Oops!", subTitle: "Your PINs do not match!", closeButtonTitle: "Okay")
                }
            }
        }else{
            let alertView = SCLAlertView()
            
            alertView.showError("Oops!", subTitle: "Your PIN must be 4 numbers!", closeButtonTitle: "Okay")
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "help" {
            let popoverViewController = segue.destination
            popoverViewController.modalPresentationStyle = UIModalPresentationStyle.popover
            popoverViewController.popoverPresentationController!.delegate = self
        }else{
            let vc : VC_SignUp_2 = segue.destination as! VC_SignUp_2
            vc.password = password
            vc.username = username
            vc.PIN = num
            vc.org = self.org
        }
    }
}
