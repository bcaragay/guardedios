//
//  Contact.swift
//  SafeTrek
//
//  Created by Bryan Caragay on 1/13/16.
//  Copyright © 2016 Bryan Caragay. All rights reserved.
//

import Foundation

open class Contact{
    var name : String = ""
    var num : String = ""

    init(name: String, num : String){
        self.name = name;
        self.num = num;
    }
}
