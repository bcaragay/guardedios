//
//  VC_Option.swift
//  SafeTrek
//
//  Created by Bryan Caragay on 2/16/16.
//  Copyright © 2016 Bryan Caragay. All rights reserved.
//

import UIKit
import Firebase
import MessageUI
import CoreLocation

class VC_Option: UITableViewController, UIPopoverPresentationControllerDelegate, MFMessageComposeViewControllerDelegate {

    let ref = Database.database().reference()
    
    //The org the current use is using
    var org : String = ""
    
    var headerText = ["Emergency Contacts", "One Touch Hotlines", "Be Yourself", "Legal Stuff"]
    
    @IBOutlet var hotline1: UITableViewCell!
    @IBOutlet var hotline2: UITableViewCell!
    
    var hotline1Num : String = ""
    var hotline1Title : String = "Error Loading Hotline"
    
    var hotline2Num : String = ""
    var hotline2Title : String = "Error Loading Hotline"
    
    override func viewWillAppear(_ animated: Bool) {
        self.hotline1.textLabel?.text = hotline1Title
        self.hotline2.textLabel?.text = hotline2Title
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewDidLoad() {super.viewDidLoad()}
    override func didReceiveMemoryWarning() {super.didReceiveMemoryWarning()}
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch((indexPath as NSIndexPath).section){
        case 0:
            switch(indexPath.row){
            case 0:
                //Search contacts
                performSegue(withIdentifier: "search", sender: self)
                break
            case 1:
                //Report incident (Calls campus police, hotline 1)
                performSegue(withIdentifier: "report", sender: self)
//                guard let url = URL(string: "telprompt://+" + hotline1Num) else { return }
//                UIApplication.shared.open(url, options: [:], completionHandler: nil)
                break
            default:
                break
            }
            
            break
            
        case 1:
            //Updating the hotline uses
            ref.child("/DataCollection/" + org + "/Hotlines").observeSingleEvent(of: .value, with: { (snapshot) in
                if(snapshot.exists()){
                    var currentUses = Int(snapshot.value as! Int)
                    currentUses += 1
                    
                    let newUses = ["Hotlines" : currentUses]
                    self.ref.child("DataCollection").child(self.org).updateChildValues(newUses)
                }
            })
            
            switch(indexPath.row){
            //Hotline #1
            case 0:
                if(hotline1.textLabel?.text != "Error Loading Hotline"){
                    guard let url = URL(string: "telprompt://+" + hotline1Num) else { return }
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                }
                break
            //Hotline #2
            case 1:
                
                if(hotline2.textLabel?.text != "Error Loading Hotline"){
                    guard let url = URL(string: "tel://+" + hotline2Num) else { return }
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                }
                break
                
            //Ntl sexual ass
            case 2:
                guard let url = URL(string: "tel://+18006564673") else { return }
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
                break
                
            //Ntl suicide prev
            case 3:
                guard let url = URL(string:  "tel://+18002738255") else { return }
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
                break
                
            //Ntl domestic violence
            case 4:
                guard let url = URL(string: "tel://+18007997233") else { return }
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
                break
            default:
                break
            }
            break;
        case 2:
            //Change School
            if(indexPath.row == 0){
                let alertView = SCLAlertView()
                
                alertView.showInfo("Oops!",
                                   subTitle: "This feature is not available yet!",
                                   closeButtonTitle: "Okay" )
            }
            //Profile
            if(indexPath.row == 1){
                
            }
            //Log Out
            
            
            break;
        case 3:
            if(indexPath.row == 0){
                try! Auth.auth().signOut()
                _ = self.navigationController?.popToRootViewController(animated: true)
            }
            
            //Terms of Service
            if(indexPath.row == 1){
                UIApplication.shared.open(URL(string: "http://scrappytechnologies.com/guarded-terms-service/")!)
            }
            
            //Privacy Statement
            if(indexPath.row == 2){
                UIApplication.shared.open(URL(string: "http://scrappytechnologies.com/privacy/")!)
            }
            break
        default:
            break;
        }
    }
    
    @IBAction func openProfile(_ sender: Any) {
        performSegue(withIdentifier: "profile", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "search"){
            let vc : SearchContacts = segue.destination as! SearchContacts
            vc.org = self.org
        }
        if(segue.identifier == "profile"){
            let vc : VC_Profile = segue.destination as! VC_Profile
            vc.org = self.org
        }
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult){
        
        switch (result) {
            case .cancelled:
                break
            case .failed:
                break
            case .sent:
                break
            }
        
        self.dismiss(animated: true) { () -> Void in
        }
    }
}
