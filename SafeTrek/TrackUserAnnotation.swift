//
//  TrackUserAnnotation.swift
//  SafeTrek
//
//  Created by Bryan Caragay on 10/11/16.
//  Copyright © 2016 Bryan Caragay. All rights reserved.
//

import UIKit
import MapKit

class TrackUserAnnotation: MKPointAnnotation {
    var lastUpdated: String!
    var imageName: String!
    
}
