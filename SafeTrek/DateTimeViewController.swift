//
//  DateTimeViewController.swift
//  SafeTrek
//
//  Created by Bryan Caragay on 10/13/17.
//  Copyright © 2017 Bryan Caragay. All rights reserved.
//

import UIKit

class DateTimeViewController: UIViewController{

    @IBOutlet var datePicker: UIPickerView!
    @IBOutlet var timePicker: UIPickerView!
    
    var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
    
    var days = ["1st" , "2nd", "3rd", "4th", "5th", "6th", "7th", "8th", "9th", "10th", "11th", "12th"]
    
    var hours = [String]()
    
    var minutes = [String]()
    
    override func viewWillAppear(_ animated: Bool) {
        for i in 0 ..< 60 {
            if(i < 10){
                minutes.append("0" + i.description)
            }else{
                minutes.append(i.description)
            }
        }
        
        for j in 1 ..< 13 {
            hours.append(j.description)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
