//
//  VC_Edit_PIN.swift
//  SafeTrek
//
//  Created by Bryan Caragay on 3/2/16.
//  Copyright © 2016 Bryan Caragay. All rights reserved.
//

import UIKit
import Firebase

class VC_Edit_PIN: UIViewController {
    @IBOutlet var pinDisplay: UILabel!
    var num : String = ""
    var canEdit = false
    var curPIN = ""
    var uid = ""
    var org : String = ""
    @IBOutlet var titleText: UILabel!
    
    @IBOutlet var lockImg: UIImageView!
    @IBOutlet var PINField: UITextField!
    @IBOutlet var dotImg: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        //Getting the users PIN number
        let ref = Database.database().reference()
        
        if let user = Auth.auth().currentUser{
            self.uid = user.uid
        }
        
        ref.child("/users/").child(org).child(uid + "/PIN").observeSingleEvent(of: .value, with: { snapshot in
            print(snapshot.value!)
            self.curPIN = snapshot.value! as! String
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func closeButton(){
        self.navigationController?.isNavigationBarHidden = false
        _ = self.navigationController?.popViewController(animated: true)
    }

    
    @IBAction func submit(_ sender: AnyObject) {
        if(canEdit && num.count == 4){ //User is editing the new PIN & has entered 4 numbers
            let ref = Database.database().reference().child("users").child(org)
            
            let n1 = ["PIN": num]
            let refc = ref.child(self.uid)
            refc.updateChildValues(n1)
            
            //Set up popup notification
            let appearance = SCLAlertView.SCLAppearance(
                showCloseButton: false // hide default button
            )
            
            let alertView = SCLAlertView(appearance: appearance)
            alertView.addButton("Okay", target:self, selector:#selector(VC_Edit_PIN.closeButton))
            alertView.showSuccess("PIN Changed!", subTitle: "Use this new PIN to cancel alerts from now on." )
        }else if(canEdit){
            if(num.count < 4){
                SCLAlertView().showError("Oops!", subTitle: "Your PIN must be 4 numbers in length.", closeButtonTitle: "Okay")
            }
        }else{ //User is entering the current PIN
            if(self.num == curPIN){ //Checking if user entered the correct PIN
                canEdit = true;
                num = ""
                titleText.text = "Enter New PIN"
                dotImg.image = UIImage(named: "pin_0")
            }else{
                dotImg.image = UIImage(named: "pin_0")
                num = ""
                SCLAlertView().showError("Oops!", subTitle: "Please check your PIN and try again!", closeButtonTitle: "Okay")
            }
        }
    }
    @IBAction func clear(_ sender: AnyObject) {
        if(num.count > 0){
            num.remove(at: num.index(before: num.endIndex))
            dotImg.image = UIImage(named: "pin_" + (num.count).description)

            //pinDisplay.text = num
        }
    }
    
    @IBAction func one(_ sender: AnyObject) {
        if(num.count < 4){
            
            num += "1"
            //pinDisplay.text = num
            check()
        }
    }
    @IBAction func two(_ sender: AnyObject) {
        if(num.count < 4){
            
            num += "2"
            //pinDisplay.text = num
            check()
        }
    }
    @IBAction func three(_ sender: AnyObject) {
        if(num.count < 4){
            
            num += "3"
           // pinDisplay.text = num
            check()
        }
    }
    @IBAction func four(_ sender: AnyObject) {
        if(num.count < 4){
            
            num += "4"
           // pinDisplay.text = num
            check()
        }
    }
    @IBAction func five(_ sender: AnyObject) {
        if(num.count < 4){
            
            num += "5"
          //  pinDisplay.text = num
            check()
        }
    }
    @IBAction func six(_ sender: AnyObject) {
        if(num.count < 4){
            
            num += "6"
          //  pinDisplay.text = num
            check()
        }
        
    }
    @IBAction func seven(_ sender: AnyObject) {
        if(num.count < 4){
            num += "7"
         //   pinDisplay.text = num
            check()
        }
        
    }
    @IBAction func eight(_ sender: AnyObject) {
        if(num.count < 4){
            num += "8"
         //   pinDisplay.text = num
            check()
        }
    }
    @IBAction func nine(_ sender: AnyObject) {
        if(num.count < 4){
            num += "9"
         //   pinDisplay.text = num
            check()
        }
    }
    @IBAction func zero(_ sender: AnyObject) {
        if(num.count < 4){
            num += "0"
            //pinDisplay.text = num
            check()
        }
    }
    func check(){
        dotImg.image = UIImage(named: "pin_" + (num.count).description)
        if(num.count == 4){
            if(num == curPIN){
                print("Match")
            }
        }
    }
}
