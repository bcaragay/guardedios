//
//  VC_ResetPassword.swift
//  SafeTrek
//
//  Created by Bryan Caragay on 4/7/16.
//  Copyright © 2016 Bryan Caragay. All rights reserved.
//

import UIKit
import Firebase

class VC_ResetPassword: UIViewController, UITextFieldDelegate {

    @IBOutlet var b_reset: UIButton!
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet var email: UITextField!

    @objc func closeButton(){
        self.navigationController?.isNavigationBarHidden = false
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func reset(_ sender: AnyObject) {
        
        Auth.auth().sendPasswordReset(withEmail: email.text!) { error in
            if error != nil {
                // There was an error processing the request
            } else {
                
                //Set up popup notification
                let appearance = SCLAlertView.SCLAppearance(
                    showCloseButton: false // hide default button
                )
                
                let alertView = SCLAlertView(appearance: appearance)
                alertView.addButton("Okay", target:self, selector:#selector(VC_ResetPassword.closeButton))
                
                alertView.showSuccess("Request Sent!", subTitle: "Check your email for instructions to reset your password." )
            }
        }
    }
}
