//
//  NoContactsCell.swift
//  SafeTrek
//
//  Created by Bryan Caragay on 10/12/16.
//  Copyright © 2016 Bryan Caragay. All rights reserved.
//

import UIKit

class NoContactsCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
