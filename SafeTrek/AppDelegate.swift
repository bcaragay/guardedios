//
//  AppDelegate.swift
//  SafeTrek
//
//  Created by Bryan Caragay on 1/13/16.
//  Copyright © 2016 Bryan Caragay. All rights reserved.
//

import UIKit
import UserNotifications

import CoreData
import Contacts
import GoogleMaps
import GooglePlaces
import Firebase
import FirebaseInstanceID
import FirebaseMessaging

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate{
    
    var username = ""
    var window: UIWindow?
    var contactStore = CNContactStore()
    let locationManager = CLLocationManager() // Add this statement

    func showMessage(_ message: String) {
        let alertController = UIAlertController(title: "Guarded", message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        let dismissAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) -> Void in
        }
        
        alertController.addAction(dismissAction)
        
        let pushedViewControllers = (self.window?.rootViewController as! UINavigationController).viewControllers
        let presentedViewController = pushedViewControllers[pushedViewControllers.count - 1]
        
        presentedViewController.present(alertController, animated: true, completion: nil)
    }
    
    class func getAppDelegate() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    func isKeyPresentInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
    
    func handleEvent(forRegion region: CLRegion!) {
        print("handling event")
        if(isKeyPresentInUserDefaults(key: "goingHome")){
            if((UserDefaults.standard.bool(forKey: "HomeLocationLat")) == true && UserDefaults.standard.integer(forKey: "goingHome") == 1){
                let twilioSID = "AC845be49806126a693312c3762980a45d"
                let twilioSecret = "ec8d333887abcd8f2098a0f3a3f4c69d"
                
                let fromNumber = "7348905855"
                var message = ""
                
                if(isKeyPresentInUserDefaults(key: "GoHomeUserName")){
                    username = UserDefaults.standard.string(forKey: "GoHomeUserName")!
                }
                
                let localNotification = UILocalNotification()
                localNotification.fireDate = NSDate(timeIntervalSinceNow: 5) as Date
                localNotification.alertBody = "Entered Destination Area. " + username + " has been alerted!"
                localNotification.timeZone = NSTimeZone.default
                localNotification.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber + 1
                
                UIApplication.shared.scheduleLocalNotification(localNotification)
                
                UserDefaults.standard.set(0, forKey: "goingHome")
                
                var toNum = ""
                if(isKeyPresentInUserDefaults(key: "GoHomeUserName")){
                    toNum = UserDefaults.standard.string(forKey: "GoHomePhoneNumber")!
                }
                
                var name = ""
                if(isKeyPresentInUserDefaults(key: "Username")){
                    name = UserDefaults.standard.string(forKey: "Username")!
                }
                
                message = "Guarded App: " + name + " is has arrived safe! No need to worry."
                 
                let request = NSMutableURLRequest(url: URL(string:"https://\(twilioSID):\(twilioSecret)@api.twilio.com/2010-04-01/Accounts/\(twilioSID)/SMS/Messages")!)
                request.httpMethod = "POST"
                request.httpBody = "From=\(fromNumber)&To=\(toNum)&Body=\(message)".data(using: String.Encoding.utf8)
                
                URLSession.shared.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
                    if let data = data, let _ = NSString(data: data, encoding: String.Encoding.utf8.rawValue) {
                    } else {
                        print("Error: \(String(describing: error))")
                    }
                }).resume()
            }
        }
    }
    
    func getUserData() {
        let user = Auth.auth().currentUser
        let email = user?.email
        let org = checkEmailExtention(s: email!)
        Database.database().reference().child("users").child(org).child((user?.uid)!).observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            let value = snapshot.value as? NSDictionary
            let username = value?["full_name"] as? String ?? ""
            print("Username: " + username)
            self.username = username
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    //Get the email extension (cmich, emich, etc)
    func checkEmailExtention(s: String) -> String {
        var v = s.components(separatedBy: "@").last?.components(separatedBy: ".")
        v?.removeLast()
        print("Email Extension: " + (v!.last)!)
        return (v!.last)!
    }

    
    func application(_ application: UIApplication,
                     didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register: \(error)")
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let token = Messaging.messaging().fcmToken
        print("FCM token: \(token ?? "")")
        
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        
        let token1 = tokenParts.joined()
        print("Device Token: \(token1)")
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        let token = Messaging.messaging().fcmToken
        print("FCM token: \(token ?? "")")
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }

    func registerForPushNotifications() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
            (granted, error) in
            print("Permission granted: \(granted)")
            
            guard granted else { return }
            self.getNotificationSettings()
        }
    }
    
    func getNotificationSettings() {
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            print("Notification settings: \(settings)")
            guard settings.authorizationStatus == .authorized else { return }
        }
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        
        //MARK: Notifications
        registerForPushNotifications()
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        Messaging.messaging().delegate = self
        
       
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        
        if(Auth.auth().currentUser != nil){
            print("Current User:" + (Auth.auth().currentUser?.uid)!)
            getUserData()

        }
        // [END register_for_notifications]
        
        
        GMSServices.provideAPIKey("AIzaSyDmcpd3RCW3cfIp6Pl984RH3M8Urx009-I")
        GMSPlacesClient.provideAPIKey("AIzaSyDmcpd3RCW3cfIp6Pl984RH3M8Urx009-I")

        UIApplication.shared.statusBarStyle = .lightContent
    
        sleep(1)
        
        return true
    }
    
    
    //InstanceID token: fcWFwBHdO5c:APA91bGeAvHg3S1m685ajJubteZMZxq_kEdCwJPumAuYlL7pey8PFiUOqlzSTGBpJBE-vAKA1qLTdw9LVntgXuHoQAiSxtNa4wHYE9cpaGsuMtQpHiq55HiqBx9xVbEWeI2vFY7cYZCs
    
    // [END connect_to_fcm]

    func applicationDidBecomeActive(_ application: UIApplication) {
        
    }
    
    // [START disconnect_from_fcm]
    func applicationDidEnterBackground(_ application: UIApplication) {
        UIApplication.shared.applicationIconBadgeNumber = 0

        print("fir from FCM.")
    }
    // [END disconnect_from_fcm]

    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
        UIApplication.shared.applicationIconBadgeNumber = 0

    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    // MARK: - Core Data stack
    
    lazy var applicationDocumentsDirectory: URL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.BryanCaragay.SafeTrek" in the application's documents Application Support directory.
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1]
    }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = Bundle.main.url(forResource: "SafeTrek", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("SingleViewCoreData.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject?
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?
            
            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()
    
    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }
}

extension AppDelegate: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        if region is CLCircularRegion {
            handleEvent(forRegion: region)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        if region is CLCircularRegion {
            handleEvent(forRegion: region)
        }
    }
}
