import UIKit
import Firebase
import CoreLocation

class VC_Send_Alert: UIViewController,CLLocationManagerDelegate {

    let ref = Database.database().reference()

    var location : CLLocationCoordinate2D!
    let locationManager = CLLocationManager()
    var contacts : [String] = []
    
    var num : String = ""
    var timer = 10
    var countdownTimer = Timer()
    
    var name : String = ""
    var phoneNum = ""
    var curPIN = ""
    var uid = ""
    var org = ""
  
    @IBOutlet var countdown: UILabel!
    @IBOutlet var code: UILabel!
    @IBOutlet var lockImg: UIImageView!
    @IBOutlet var pinImg: UIImageView!
    
    override func viewWillAppear(_ animated: Bool) {
        //Begin the countdown timer
        self.countdownTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(VC_Send_Alert.countdowns), userInfo: nil, repeats: true)
        
        //Get the UID from the current user
        if let user = Auth.auth().currentUser{uid = user.uid}
        
        
        //Quering the users contacts
        let ref4 = ref.child("/users/").child(org).child(uid + "/contacts")
        ref4.queryOrderedByKey().observe(.childAdded, with: { snapshot in
            let c = (snapshot.value as? String)!
            print(c)
            self.contacts.append(c)
        })
        
        //Getting the users PIN number
        let ref2 = ref.child("/users/").child(org).child(uid + "/PIN")
        ref2.observeSingleEvent(of: .value, with: { snapshot in
            self.curPIN = snapshot.value as! String
            print("PIN : " + self.curPIN)
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let user = Auth.auth().currentUser{
            //Getting the users name
            let ref2 = ref.child("/users/").child(org).child(user.uid + "/full_name")

            ref2.observeSingleEvent(of: .value, with: { snapshot in
                self.name = snapshot.value as! String
            })
            
            //Getting the users phone number
            let refNum = ref.child("/users/").child(org).child(user.uid + "/phone_number")
            
            refNum.observeSingleEvent(of: .value, with: { snapshot in
                self.phoneNum = snapshot.value as! String
            })
        }

        navigationController?.isNavigationBarHidden = true
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.distanceFilter = 1.0
            locationManager.startUpdatingLocation()
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        location = locValue
    }
    
    
    /// Method to actually count down from 10.
    @objc func countdowns() {
        if(timer == 0){countdownTimer.invalidate()}
        
        if(timer != 0 && num.description != curPIN){
            timer -= 1
            countdown.text = timer.description + " seconds"
        }else if(timer <= 0){
            
            countdownTimer.invalidate()
            sendSMS()
            
            if(CLLocationManager.locationServicesEnabled()){
                //Set up popup notification
                let appearance = SCLAlertView.SCLAppearance(
                    showCloseButton: false // hide default button
                )
                
                //This can break if the organization doens't have data tracking set up in Firebase
                self.ref.child("/DataCollection/" + self.org + "/AlertsSent").observeSingleEvent(of: .value, with: { (snapshot) in
                    var currentUses = Int(snapshot.value as! Int)
                    currentUses += 1
                    
                    let newUses = ["AlertsSent" : currentUses]
                    self.ref.child("DataCollection").child(self.org).updateChildValues(newUses)                    
                })
                
                let alertView = SCLAlertView(appearance: appearance)
                alertView.addButton("Okay", target:self, selector:#selector(VC_Send_Alert.closeButton))
                alertView.showInfo("Alert Sent!", subTitle: "Your contacts have been alerted and informed of your exact postion." )
            }
        }
    }
    
    @objc func closeButton(){
        self.navigationController?.isNavigationBarHidden = false
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    //Check every time the user hits a number to see if it matches the user's PIN
    func check(){
        pinImg.image = UIImage(named: "pin_" + (num.count.description))
        if(num == curPIN){
            countdownTimer.invalidate()
            //Set up popup notification
            let appearance = SCLAlertView.SCLAppearance(
                showCloseButton: false // hide default button
            )
            
            let alertView = SCLAlertView(appearance: appearance)
            alertView.addButton("Okay", target:self, selector:#selector(VC_Send_Alert.closeButton))
            
            alertView.showSuccess("Alert Canceled!", subTitle: "We are glad you are safe." )
            
        }else{
            if(num.count == 4){
                num = ""
                pinImg.image = UIImage(named: "pin_" + (num.count.description))
            }
        }
    }
    
    
    /// Sending the actual alert if the PIN is not entered in 10 seconds
    func sendSMS()
    {
        let twilioSID = "AC845be49806126a693312c3762980a45d"
        let twilioSecret = "ec8d333887abcd8f2098a0f3a3f4c69d"
        
        let fromNumber = "7348905855"
        var message = ""
        
        let uuid = UUID().uuidString
        
        var userInfo = [String : String]()
        
        if CLLocationManager.locationServicesEnabled() {

            userInfo = ["UID" : (Auth.auth().currentUser?.uid.description)!,
                        "lat" : self.location.latitude.description,
                        "long" : self.location.longitude.description,
                        "phone" : self.phoneNum,
                        "name" : self.name,
                        "email" : (Auth.auth().currentUser?.email?.description)!,
                        "closed" : "false"]
        }else{
            userInfo = ["UID" : (Auth.auth().currentUser?.uid.description)!,
                        "phone" : self.phoneNum,
                        "email" : (Auth.auth().currentUser?.email?.description)!,
                        "name" : self.name,
                        "closed" : "false"]
        }
        
        self.ref.child("MapAlerts").child(self.org).child(uuid).updateChildValues(userInfo)
        
        for i in contacts{
            
            //Set the message depending on if the user has location services enabled
            if CLLocationManager.locationServicesEnabled() {
                message = "Guarded App: " + name + " is alerting you: http://www.guardedapp.io/map.html?org=" + org + ",key=" + uuid
            }else{
                message = "Guarded App: You are recieving this because " + name + " is alerted you from the Guarded Safety app! Contact info: " + phoneNum
            }
            
            
            let request = NSMutableURLRequest(url: URL(string:"https://\(twilioSID):\(twilioSecret)@api.twilio.com/2010-04-01/Accounts/\(twilioSID)/SMS/Messages")!)
            request.httpMethod = "POST"
            request.httpBody = "From=\(fromNumber)&To=\(i)&Body=\(message)".data(using: String.Encoding.utf8)
          
            URLSession.shared.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
                if let data = data, let responseDetails = NSString(data: data, encoding: String.Encoding.utf8.rawValue) {
                    print("Response: \(responseDetails)")
                } else {
                    print("Error: \(String(describing: error))")
                }
            }).resume()
        }
    }
    
    //MARK: Keypad Inputs

    @IBAction func sendAlertNow(_ sender: Any) {
        countdownTimer.invalidate()
        sendSMS()
        
        if(CLLocationManager.locationServicesEnabled()){
            //Set up popup notification
            let appearance = SCLAlertView.SCLAppearance(
                showCloseButton: false // hide default button
            )
            
            //This can break if the organization doens't have data tracking set up in Firebase
            self.ref.child("/DataCollection/" + self.org + "/AlertsSent").observeSingleEvent(of: .value, with: { (snapshot) in
                var currentUses = Int(snapshot.value as! Int)
                currentUses += 1
                
                let newUses = ["AlertsSent" : currentUses]
                self.ref.child("DataCollection").child(self.org).updateChildValues(newUses)
            })
            
            let alertView = SCLAlertView(appearance: appearance)
            alertView.addButton("Okay", target:self, selector:#selector(VC_Send_Alert.closeButton))
            alertView.showInfo("Alert Sent!", subTitle: "Your contacts have been alerted and informed of your exact postion." )
        }
    }
    
    @IBAction func one(_ sender: AnyObject) {
        if(num.count < 4){
            num += "1"
            check()
        }
    }
    @IBAction func two(_ sender: AnyObject) {
        if(num.count < 4){
            num += "2"
            check()
        }
    }
    
    @IBAction func three(_ sender: AnyObject) {
        if(num.count < 4){
            num += "3"
            check()
        }
    }
    
    @IBAction func four(_ sender: AnyObject) {
        if(num.count < 4){
            num += "4"
            check()
        }
    }
    
    @IBAction func five(_ sender: AnyObject) {
        if(num.count < 4){
            num += "5"
           // code.text = num
            check()
        }
    }
    
    @IBAction func six(_ sender: AnyObject) {
        if(num.count < 4){
            num += "6"
            check()
        }
    }
    
    @IBAction func seven(_ sender: AnyObject) {
        if(num.count < 4){
            num += "7"
            check()
        }
    }
    
    @IBAction func eight(_ sender: AnyObject) {
        if(num.count < 4){
            num += "8"
            check()
        }
    }
    
    @IBAction func nine(_ sender: AnyObject) {
        if(num.count < 4){
            num += "9"
            check()
        }
    }
    
    @IBAction func zero(_ sender: AnyObject) {
        if(num.count < 4){
            num += "0"
            check()
        }
    }
    
    @IBAction func back(_ sender: AnyObject) {
        if(num.count > 0){
            num.remove(at: num.index(before: num.endIndex))
            pinImg.image = UIImage(named: "pin_" + (num.count.description))
        }
    }

    func getCurrentLocalDate()-> String {
        var now = Date()
        var nowComponents = DateComponents()
        let calendar = Calendar.current
        nowComponents.year = (Calendar.current as NSCalendar).component(NSCalendar.Unit.year, from: now)
        nowComponents.month = (Calendar.current as NSCalendar).component(NSCalendar.Unit.month, from: now)
        nowComponents.day = (Calendar.current as NSCalendar).component(NSCalendar.Unit.day, from: now)
        nowComponents.hour = (Calendar.current as NSCalendar).component(NSCalendar.Unit.hour, from: now)
        nowComponents.minute = (Calendar.current as NSCalendar).component(NSCalendar.Unit.minute, from: now)
        nowComponents.second = (Calendar.current as NSCalendar).component(NSCalendar.Unit.second, from: now)
        (nowComponents as NSDateComponents).timeZone = TimeZone(abbreviation: "EDT")
        now = calendar.date(from: nowComponents)!
        
        let formatter = DateFormatter()
        formatter.dateStyle = DateFormatter.Style.long
        formatter.timeStyle = .medium
        
        let dateString = formatter.string(from: now)
        return dateString
    }
    
}
