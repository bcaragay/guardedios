//
//  ContactCell.swift
//  SafeTrek
//
//  Created by Bryan Caragay on 1/25/16.
//  Copyright © 2016 Bryan Caragay. All rights reserved.
//

import UIKit

class ContactCell: UITableViewCell {

    @IBOutlet var numberLabel: UILabel!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var contactImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        contactImage.layer.cornerRadius = contactImage.frame.size.height / 2;
        contactImage.layer.masksToBounds = true;
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
