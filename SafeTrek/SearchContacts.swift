//
//  SearchContacts.swift
//  SafeTrek
//
//  Created by Bryan Caragay on 1/13/16.
//  Copyright © 2016 Bryan Caragay. All rights reserved.
//

import UIKit
import Contacts
import Firebase

class SearchContacts: UITableViewController, UISearchBarDelegate {

    @IBOutlet var searchBar: UISearchBar!
    
    var store = CNContactStore()
    
    //Contacts that are added to this are during user searching
    var searchingContacts : [CNContact] = []
    
    //All the user's contacts
    var allContacts : [CNContact] = []

    //Is the user currently searching
    var searchActive : Bool = false
    
    //Current users's organization
    var org : String = ""
    
    //The items from Firebase, so the user can delete them.
    var items : [DataSnapshot] = []
    
    @IBOutlet var topRightButtonItem: UIBarButtonItem!
    
    var showPermissionAlert = false
    
    //The names of the items from the above array, so the cells can access them
    var contacts : [Contact] = []
    let refMain = Database.database().reference()
    var uid = ""
    
    //We reuse the same code to set the destination finder contacts, so check which we are doing.
    var setHomeFlag = false
    
    var selectedRow : Int = 0
    
    @IBAction func addButton(_ sender: AnyObject) {
        UIView.animate(withDuration: 0.4, animations: {
            self.tableView.contentInset = UIEdgeInsets(top: 105, left: 0, bottom: 0, right: 0)
            self.searchBar.becomeFirstResponder()
        })
    }
    
    func requestContactAccess() {
        let authorizationStatus = CNContactStore.authorizationStatus(for: CNEntityType.contacts)

        switch authorizationStatus {
        case .authorized:
            self.showPermissionAlert = false
            break
        case .denied, .notDetermined:
            self.showPermissionAlert = true
            self.store.requestAccess(for: CNEntityType.contacts, completionHandler: { (access, accessError) -> Void in
                if access {
                    self.showPermissionAlert = false
                }
            })
            
        default: break
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        requestContactAccess()
        
        if(!setHomeFlag){
            if let user = Auth.auth().currentUser{
                uid = user.uid
            }
            
            let ref = refMain.child("/users/").child(org).child(self.uid + "/contacts")
            
            //Adding the contacts to the array, to be able to access names and numbers.
            ref.queryOrderedByKey().observe(.childAdded, with: { snapshot in
                let c = Contact(name: (snapshot.key), num: (snapshot.value as? String)!)
                self.contacts.append(c)
                self.contacts = self.contacts.sorted { $0.name < $1.name }
                self.tableView.reloadData()
            })
            
            //Adding the items to the object array to be able to delete them from Firebase
            ref.observe(.value, with: { snapshot in
                var newItems = [DataSnapshot]()
        
                for item in snapshot.children {
                    let groceryItem = item as! DataSnapshot
                    newItems.append(groceryItem)
                }
                
                self.items = newItems
                self.tableView.reloadData()
            })
        }else{
            let requestForContacts = CNContactFetchRequest(keysToFetch: [ CNContactFormatter.descriptorForRequiredKeys(for: CNContactFormatterStyle.fullName), CNContactPhoneNumbersKey as CNKeyDescriptor ,CNContactImageDataKey as CNKeyDescriptor])

            do{
                try self.store.enumerateContacts(with: requestForContacts) { contact, stop in
                    self.allContacts.append(contact)
                }
            } catch let err{
                print(err)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        self.tableView.contentInset = UIEdgeInsets(top: -45, left: 0, bottom: 0, right: 0)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        let auth = CNContactStore.authorizationStatus(for: CNEntityType.contacts)

        if(auth == .notDetermined || auth == .restricted || auth == .denied){
            //The user has not allowed Guarded access to thier contacts
            if(showPermissionAlert){
                searchBar.text = ""
                
                let appearance = SCLAlertView.SCLAppearance(showCloseButton: false)
                
                let alert = SCLAlertView(appearance: appearance)
                
                alert.addButton("Okay!") {self.showPermissionAlert = true}
                
                self.showPermissionAlert = false
                alert.showError("Need Permissions!", subTitle: "You must allow Guarded access to your contacts. Settings -> Guarded -> Enable Contacts")
            }
        }else{
            
            filterContacts(searchText)
            
            if(searchingContacts.count == 0 || searchBar.text == ""){
                searchActive = false;
            } else {
                searchActive = true;
            }
            
            self.tableView.reloadData()
        }
    }
    
    func filterContacts(_ name : String){
        searchingContacts.removeAll()
        
        let predicate = CNContact.predicateForContacts(matchingName: name)
        let toFetch = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey, CNContactImageDataKey, CNContactImageDataAvailableKey]
        
        do{
            let contacts = try store.unifiedContacts(matching: predicate, keysToFetch: toFetch as [CNKeyDescriptor])
            for contact in contacts{
                searchingContacts.append(contact)
            }
        } catch let err{
            print(err)
        }
    }
    
    func getContactImage(_ name : String) -> Data{
        let predicate = CNContact.predicateForContacts(matchingName: name)
        let toFetch = [CNContactImageDataKey, CNContactImageDataAvailableKey]
        
        do{
            let contacts = try store.unifiedContacts(
                matching: predicate, keysToFetch: toFetch as [CNKeyDescriptor])
            
            for contact in contacts{
                if(contact.imageDataAvailable){
                    return contact.imageData!
                }else{
                    let data = "".data(using: String.Encoding.utf8)
                    return data!
                }
            }
            
        } catch let err{
            print(err)
        }
        
        let data = "".data(using: String.Encoding.utf8)
        return data!
    }

    //================================================================================
    //# MARK: - TableView Methods
    //================================================================================
    
    override func numberOfSections(in tableView: UITableView) -> Int {return 1}

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(setHomeFlag){
            if(searchActive){
                return searchingContacts.count
            }
            return allContacts.count
        }else if(searchActive) {
            return searchingContacts.count
        }else{
            if(contacts.count == 0){
                return 1
            }else{
                return contacts.count
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(setHomeFlag){
            return 84
        }
        if(searchActive){
            return 84
        }else{
            if(contacts.count == 0){
                return 288
            }else{
                return 84
            }
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(setHomeFlag){
            if(searchActive){
                let cell = tableView.dequeueReusableCell(withIdentifier: "addCell", for: indexPath) as! AddCell
                
                cell.accessoryType = UITableViewCellAccessoryType.none
                
                print(searchingContacts.count)
                if(searchingContacts[(indexPath as NSIndexPath).row].phoneNumbers.count > 0){
                    cell.numberLabel?.text = ((searchingContacts[(indexPath as NSIndexPath).row].phoneNumbers[0] ).value ).stringValue
                }
                cell.nameLabel?.text = searchingContacts[(indexPath as NSIndexPath).row].givenName + " " + searchingContacts[(indexPath as NSIndexPath).row].familyName
                
                if let imageData = searchingContacts[(indexPath as NSIndexPath).row].imageData{
                   // cell.contactImage.image = UIImage(data: imageData)
                } else {
                }
                
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "addCell", for: indexPath) as! AddCell
                
                cell.accessoryType = UITableViewCellAccessoryType.none
                
                if(allContacts[(indexPath as NSIndexPath).row].phoneNumbers.count > 0){
                    cell.numberLabel?.text = ((allContacts[(indexPath as NSIndexPath).row].phoneNumbers[0] ).value ).stringValue
                }
                cell.nameLabel?.text = allContacts[(indexPath as NSIndexPath).row].givenName + " " + allContacts[(indexPath as NSIndexPath).row].familyName
                
                if let imageData = allContacts[(indexPath as NSIndexPath).row].imageData{
                    cell.contactImage.image = UIImage(data: imageData)
                }
                
                return cell
            }
        }else if(searchActive){
            let cell = tableView.dequeueReusableCell(withIdentifier: "addCell", for: indexPath) as! AddCell
            
            cell.accessoryType = UITableViewCellAccessoryType.none

            if(searchingContacts[(indexPath as NSIndexPath).row].phoneNumbers.count > 0){
                cell.numberLabel?.text = ((searchingContacts[(indexPath as NSIndexPath).row].phoneNumbers[0] ).value ).stringValue
            }
            cell.nameLabel?.text = searchingContacts[(indexPath as NSIndexPath).row].givenName + " " + searchingContacts[(indexPath as NSIndexPath).row].familyName
        
            if let imageData = searchingContacts[(indexPath as NSIndexPath).row].imageData{
              //  cell.contactImage.image = UIImage(data: imageData)
            }

            return cell
        }else{
            if(contacts.count == 0){
                let cell = tableView.dequeueReusableCell(withIdentifier: "noContacts", for: indexPath) as! NoContactsCell
                
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "contactCell", for: indexPath) as! ContactCell
                
                cell.accessoryType = UITableViewCellAccessoryType.none
                
                
                if(contacts.count > 0){
                    cell.numberLabel?.text = contacts[(indexPath as NSIndexPath).row].num
                    cell.nameLabel?.text = contacts[(indexPath as NSIndexPath).row].name
                    //See if the contact using .name has a contact image
                    let datastring = NSString(data: getContactImage(contacts[(indexPath as NSIndexPath).row].name), encoding: String.Encoding.utf8.rawValue)
                    
                    //Contact has an image
                    if(datastring != ""){
                       // cell.contactImage.image = UIImage(data: getContactImage(contacts[(indexPath as NSIndexPath).row].name))
                    }else {
                        cell.contactImage.image = UIImage(named: "ProfilePic")
                    }
                }else{
                    tableView.reloadData()
                }
                
                return cell
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if((indexPath as NSIndexPath).row % 2 == 0){
            cell.backgroundColor = UIColor(red: 242, green: 242, blue: 242, alpha: 1)
        }else{
            cell.backgroundColor = UIColor(red: 250, green: 250, blue: 250, alpha: 1)
        }
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        if(!searchActive){
            return true
        }
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            let contact = items[(indexPath as NSIndexPath).row]
            for index in 0...contacts.count - 1 {
                if(contacts[index].num == contact.value as! String){
                    contacts.remove(at: index)
                    break
                }
            }
            
            contact.ref.removeValue()
            self.tableView.reloadData()
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let alert = UIAlertController(title: "Select Phone Number", message: "Click the phone number to add it!", preferredStyle: .alert)
        
        let alertError = UIAlertController(title: "Oh no!", message: "That contact does not have a phone number associated with it!", preferredStyle: .alert)
        
        alertError.addAction(UIAlertAction(title: "Okay", style: .default, handler: {(action: UIAlertAction!) in
            
        }))
    
        let alertView = SCLAlertView()
        
        //The user is picking a contact to set as the go home contact
        if(setHomeFlag){
            if(searchActive){
                let user = searchingContacts[(indexPath as NSIndexPath).row]
                UserDefaults.standard.set(user.givenName + " " + user.familyName, forKey: "GoHomeUserName")
                UserDefaults.standard.set(user.phoneNumbers[0].value.stringValue, forKey: "GoHomePhoneNumber")
                _ = self.navigationController?.popViewController(animated: true)
            }else{
                let user = allContacts[(indexPath as NSIndexPath).row]
                UserDefaults.standard.set(user.givenName + " " + user.familyName, forKey: "GoHomeUserName")
                UserDefaults.standard.set(user.phoneNumbers[0].value.stringValue, forKey: "GoHomePhoneNumber")
                _ = self.navigationController?.popViewController(animated: true)
            }
        }else{
            if(searchActive){
                //If the user does not have more than 5 contacts, allow them to add more.
                if(contacts.count < 5){
                    var index = 0
                    
                    for phoneNumber:CNLabeledValue in searchingContacts[(indexPath as NSIndexPath).row].phoneNumbers {
                        let phonenum = phoneNumber.value 
                        index += 1
                        let b = alertView.addButton("\(phonenum.stringValue)", target:self, selector:#selector(VC_Send_Alert.closeButton))
                        b.backgroundColor = UIColor.init(red: 217.0/255.0, green: 68.0/255.0, blue: 68.0/255.0, alpha: 1.0)

                        alert.addAction(UIAlertAction(title: "\(phonenum.stringValue)", style: .default, handler: { (action: UIAlertAction!) in
                            let ref1 = self.refMain.child("/users/").child(self.org).child(self.uid)
                            let refc = ref1.child("contacts/" )
                            
                            let name = self.searchingContacts[(indexPath as NSIndexPath).row].givenName + " " + self.searchingContacts[(indexPath as NSIndexPath).row].familyName
                            
                            //Saving in the form of "Name:Number"
                            let newContact = [name : phonenum.stringValue]
                            
                            self.searchActive = false
                            self.searchBar.text = ""
                            self.searchingContacts.removeAll()
                            refc.updateChildValues(newContact)
                        }))
                    }
                    
                    alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
                    }))
                   
                    //If there are no phone numbers associated with the contact
                    if(index == 0){
                        self.present(alertError, animated: true, completion: nil)
                    }else{
                        self.present(alert, animated: true, completion: nil)
                    }
                }else{
                    //User already has 5 contacts added, cannot add more.
                    SCLAlertView().showError("Oops", subTitle: "You can only have 5 contacts listed!", closeButtonTitle: "Okay" )
                }
            }else{
                if(contacts.count > 0){
                    //Set the selected row
                    selectedRow = (indexPath as NSIndexPath).row
                    
                                
                    let dButton = alertView.addButton("Delete", target:self, selector:#selector(SearchContacts.removeContact))

                    alertView.showInfo("Delete Contact?", subTitle: "Do you want to delete this contact?", closeButtonTitle: "Cancel" )
                    
                    dButton.backgroundColor = UIColor.init(red: 217.0/255.0, green: 68.0/255.0, blue: 68.0/255.0, alpha: 1.0)
                }
        }
        }
    }
    
    @objc func removeContact(){
        //Delete the contact selected above
        let contact = items[selectedRow]
        for index in 0...contacts.count - 1 {
            if(contacts[index].num == contact.value as! String){
                contacts.remove(at: index)
                break
            }
        }
        
        contact.ref.removeValue()
        self.tableView.reloadData()
    }
    
    //Deprecated
//    func loadContacts(){
//        let ref = refMain.child(self.uid + "/contacts")
//
//        ref.queryOrderedByKey().observe(.childAdded, with: { snapshot in
//            let c = Contact(name: (snapshot.key), num: (snapshot.value as? String)!)
//            self.contacts.append(c)
//        })
//
//        ref.observe(.value, with: { snapshot in
//            var dict = [DataSnapshot]()
//
//            for item in snapshot.children {
//                dict.append(item as! DataSnapshot)
//            }
//
//            self.items = dict
//        })
//    }
    
    override func didReceiveMemoryWarning() {super.didReceiveMemoryWarning()}
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.tableView.contentInset = UIEdgeInsets(top: 65, left: 0, bottom: 0, right: 0)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        searchBar.resignFirstResponder()
        searchActive = false;
        filterContacts("")
        
        UIView.animate(withDuration: 0.4, animations: {
            self.tableView.contentInset = UIEdgeInsets(top: (searchBar.bounds.height / 2) - 2, left: 0, bottom: 0, right: 0)
        })
        
        if(searchingContacts.count == 0){
            searchActive = false;
        } else {
            searchActive = true;
        }
        
        self.tableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}
