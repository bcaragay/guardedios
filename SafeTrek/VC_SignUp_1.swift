//
//  VC_SignUp_1.swift
//  SafeTrek
//
//  Created by Bryan Caragay on 2/18/16.
//  Copyright © 2016 Bryan Caragay. All rights reserved.
//

import UIKit
import Firebase
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func >= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l >= r
  default:
    return !(lhs < rhs)
  }
}


class VC_SignUp_1: UIViewController, UITextFieldDelegate {

    @IBOutlet var passwordRep: UITextField!
    @IBOutlet var passwordField: UITextField!
    @IBOutlet var emailField: UITextField!
    @IBOutlet var universityLabel: UILabel!
    let activityIndictor: UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
    
    var org = ""
    var orgs: [String:String] = [
        "cmich" : "Central Michigan University",
        "emich" : "Eastern Michigan University",
        "umich" : "University Of Michigan",
        "ferris": "Ferris State University"
    ]
    
    var lastPn = ""
    
    let ref = Database.database().reference()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        activityIndictor.center = view.center
        self.view.addSubview(activityIndictor)
        activityIndictor.alpha = 0

        passwordField.delegate = self
        passwordRep.delegate = self
        emailField.delegate = self
    }
    
    func displayError(_ errorMsg : String){
        SCLAlertView().showError("Oops", subTitle: errorMsg) // Error
    }
    
    @IBAction func updateOrgLabel(_ sender: Any) {
        if(emailField.text != ""){
            if(isValidEmail(emailField.text!)){
                var v = emailField.text?.components(separatedBy: "@").last?.components(separatedBy: ".")
                v?.removeLast()
                print("Email Extension: " + (v!.last)!)
                let org = (v!.last)!
                self.org = org.lowercased()
                
                universityLabel.text = orgs[org]
                
                updateUI(org: org)
            }
        }
    }
    
    @IBOutlet var nextButton: UIButton!
    @IBOutlet var createAccountLabel: UILabel!
    
    func updateUI(org : String){
        switch org.lowercased() {
        case "cmich":
            self.view.backgroundColor = #colorLiteral(red: 0.4969488978, green: 0.06781326979, blue: 0.255400002, alpha: 1)
            createAccountLabel.textColor = UIColor.white
            nextButton.backgroundColor = #colorLiteral(red: 1, green: 0.8170465827, blue: 0.23015818, alpha: 1)
            break
        case "ferris":
            self.view.backgroundColor = #colorLiteral(red: 0.7294117647, green: 0.04705882353, blue: 0.1843137255, alpha: 1)
            createAccountLabel.textColor = UIColor.white
            nextButton.backgroundColor = #colorLiteral(red: 1, green: 0.8156862745, blue: 0.262745098, alpha: 1)
        default:
            break
        }
    }
    
    func isValidEmail(_ testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func checkValidity() -> Bool{
        print("Checking validity...")
        //Check to make sure all fields are filled in
        if(passwordField.text != "" && passwordRep.text != "" && emailField.text != "" ){
            print("1")
            //Make sure password is at least 6 characters
            if(passwordField.text?.count >= 6){
                print("2")
                //Check email address
                if(isValidEmail(emailField.text!)){
                    print("3")
                    //Check matching passwords
                    if(passwordField.text != passwordRep.text){
                        print("4")
                        displayError("Passwords do not match.")
                        return false
                    }else{
                        print("3.5")
                        return true
                    }
                }else{
                    print("5")
                    displayError("invalid email address")
                    return false
                }
            }else{
                print("7.7")
                displayError("Password must be at least 6 characters!")
                return false
            }
        }else{
            print("8.5")
            displayError("All fields must be filled in!")
            return false
        }
    }
    
    @IBAction func signUp(){
        activityIndictor.alpha = 1
        activityIndictor.startAnimating()
        
        if(isValidEmail(emailField.text!)){
            var v = emailField.text?.components(separatedBy: "@").last?.components(separatedBy: ".")
            v?.removeLast()
            print("Email Extension: " + (v!.last)!)
            let org = (v!.last)!
            let refValid = ref.child("ContractStatus").child(org.lowercased())
            
            ///Checking if email is in valid email@page.com format
            if(checkValidity()){
                print("7")
                
                ref.observeSingleEvent(of: .value, with: { (snapshot) in
                    print("7.9")
                })
                
                ///Checking to see if this institution has a contract with us
                ref.child("ContractStatus").observeSingleEvent(of: .value, with: {snapshot in
                    print("8")
                    
                    if(snapshot.hasChild(org.lowercased())){
                        print("Org is registered...")
                        refValid.observeSingleEvent(of: .value, with: { snapshot in
                            if(snapshot.value as! String == "individual" || snapshot.value as! String == "open" || snapshot.value as! String == "contract"){
                                self.activityIndictor.stopAnimating()
                                self.activityIndictor.alpha = 0
                                self.performSegue(withIdentifier: "PIN", sender: self)
                            }else{
                                self.activityIndictor.stopAnimating()
                                self.activityIndictor.alpha = 0
                                self.displayError("We're not at your school yet, but we're coming soon!")
                            }
                        })
                    }else{
                        self.activityIndictor.stopAnimating()
                        self.activityIndictor.alpha = 0
                        self.displayError("We're not at your school yet, but we're coming soon!")
                    }
                })
            }
        }else{
            self.activityIndictor.stopAnimating()
            self.activityIndictor.alpha = 0
            let alert = SCLAlertView()
            alert.showError("Email Invalid", subTitle: "Your email is not valid. Check it and try again.")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc : VC_Setup_PIN = segue.destination as! VC_Setup_PIN
        vc.password = passwordField.text!
        vc.username = emailField.text!
        vc.org = self.org
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    fileprivate func addBottomLineToTextField(_ textField : UITextField) {
        let border = CALayer()
        let borderWidth = CGFloat(1.0)
        border.borderColor = UIColor.white.cgColor
        border.frame = CGRect(x: 0, y: textField.frame.size.height - borderWidth, width: textField.frame.size.width, height: textField.frame.size.height)
        border.borderWidth = borderWidth
        textField.layer.addSublayer(border)
        textField.layer.masksToBounds = true
    }

}
