//
//  VC_Profile.swift
//  SafeTrek
//
//  Created by Bryan Caragay on 2/17/16.
//  Copyright © 2016 Bryan Caragay. All rights reserved.
//

import UIKit
import Firebase

class VC_Profile: UITableViewController, UITextFieldDelegate {

    @IBOutlet var loader: UIActivityIndicatorView!
    @IBOutlet var phoneNumber: UITextField!
    @IBOutlet var fullName: UITextField!
    @IBOutlet var done: UIButton!
    @IBOutlet var fakeCallerID: UITextField!
    
    var headerText = ["My Info Stuff", "Log In Stuff"]
    var uid = ""
    var email = ""
    let ref = Database.database().reference()
    var org = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loader.startAnimating()
        phoneNumber.delegate = self
        fullName.delegate = self

        let ref2 = ref.child("users").child(org)
        
        if let user = Auth.auth().currentUser{
            uid = user.uid
            email = user.email!
        }
        
        //TET THIS STUFF
        ref2.queryOrdered(byChild: "email").observe(.childAdded, with: { snapshot in
            print(snapshot.key + " : " + self.uid)
            if(snapshot.key == self.uid){
                let value = snapshot.value as! [String:AnyObject]
                if let pn = value["phone_number"] as? String {
                    self.phoneNumber.text = pn
                }
                if let name = value["full_name"] as? String {
                    self.fullName.text = name
                }
                if let callerid = value["callerid"] as? String {
                    self.fakeCallerID.text = callerid
                }
                self.loader.stopAnimating()
                self.loader.isHidden = true
            }
        })
    }
//    
//    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        
//        let view = UIView()
//        view.backgroundColor = #colorLiteral(red: 0.9418932361, green: 0.9418932361, blue: 0.9418932361, alpha: 1)
//        
//        let label = UILabel()
//        label.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
//        
//        label.font = UIFont(name: "Roboto-Black", size: 20)
//        
//        label.text = headerText[section]
//        label.frame = CGRect(x: 10, y: 8, width: 250, height: 45)
//        view.addSubview(label)
//        
//        return view
//    }
    
//    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 55
//    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch(indexPath.section){
        case 0:
            switch indexPath.row {
                case 0:
                    fullName.becomeFirstResponder()
                    break
                case 1:
                    phoneNumber.becomeFirstResponder()
                    break
                case 2:
                    fakeCallerID.becomeFirstResponder()
                    break
            default:
                break
            }
            break
        case 1:
            switch indexPath.row{
            case 0:
                performSegue(withIdentifier: "editPIN", sender: self)
                break
            case 1:
                performSegue(withIdentifier: "editPass", sender: self)
                break
            case 2:
                performSegue(withIdentifier: "resetPass", sender: self)
                break
            default:
                break
            }
            break
        default:
            break
        }
        
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "editPIN"){
            let vc : VC_Edit_PIN = segue.destination as! VC_Edit_PIN
            vc.org = self.org
        }
        
        if(segue.identifier == "editPass"){
            let vc : VC_ChangePassword = segue.destination as! VC_ChangePassword
            vc.org = self.org
        }
    }

    @IBAction func save(_ sender: Any) {
        self.view.endEditing(true)
        let newUser = [
            "full_name" : fullName.text!,
            "phone_number" : phoneNumber.text!,
            "callerid" : fakeCallerID.text!
        ]
        
        //Updating the path /users/UID to update the above information.
        let refc = ref.child("users/").child(org).child(uid)
        refc.updateChildValues(newUser)
        
        let alert = SCLAlertView()
        alert.showSuccess("Saved", subTitle: "Your info was saved!")
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
