//
//  SubmitReportTableViewController.swift
//  Guarded
//
//  Created by Bryan Caragay on 5/4/18.
//  Copyright © 2018 Bryan Caragay. All rights reserved.
//

import UIKit
import GoogleMaps
import FirebaseDatabase
import FirebaseAuth

class SubmitReportTableViewController: UITableViewController, GMSMapViewDelegate, UITextFieldDelegate, UITextViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate {
    
    var blurEffectView: UIVisualEffectView!
    
    @IBOutlet var descriptionField: UITextView!
    @IBOutlet var nameField: UITextField!
    @IBOutlet var emailField: UITextField!
    @IBOutlet var numberField: UITextField!
    
    @IBOutlet weak var incidentPickerView: UIPickerView!
    @IBOutlet var incidentTypeLabel: UILabel!

    @IBOutlet var incidentTypeView: UIView!
    @IBOutlet var mapView: GMSMapView!
    
    var selectedLat = "", selectedLong = "", selectedType = ""
    
    var ref : DatabaseReference!

    var incidentTypes = ["Theft",
                         "Assault",
                         "Vandalism",
                         "Suspicious Behavior",
                         "Animal Problem",
                         "Robbery",
                         "Vehicle Problem",
                         "Fire",]
    
    var org = "cmich"
    
    override func viewWillAppear(_ animated: Bool) {
        ref = Database.database().reference()
        loadUserData()
        
        
        let campus = GMSCameraPosition.camera(withLatitude: 43.582273,
                                              longitude: -84.775680,
                                              zoom: 14)
        mapView.camera = campus
    }
    
    
    func textViewDidBeginEditing(_ textField: UITextView) {
        if(textField.text == "Quick description of the event...."){
            textField.text = ""
        }
    }
    
    @IBAction func submitReport(_ sender: Any) {
        if(isAllFieldsFilledIn()) {
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd:MM hh:mm"
            
            let timeZone = TimeZone.current
            
            dateFormatter.timeZone = timeZone
            dateFormatter.dateStyle = .short
            dateFormatter.timeStyle = .short
            
            let time = dateFormatter.string(from: Date())
            
            self.ref.child("IncidentReports")
                .child(org)
                .child(UUID().uuidString)
                .updateChildValues(["Name" : nameField.text!,
                                    "Phone" : numberField.text!,
                                    "Email" : emailField.text!,
                                    "Lat"   : selectedLat,
                                    "Time"  : time,
                                    "Long"  : selectedLong,
                                    "Type"  : selectedType,
                                    "Viewed": "False",
                                    "Description" : descriptionField.text])
            
            self.navigationController?.popViewController(animated: true)
        }else{
            
        }
    }
    
    func isAllFieldsFilledIn() -> Bool {
        if(nameField.text != "" && descriptionField.text != "" && emailField.text != "" && numberField.text != "" && incidentTypeLabel.text != "Select Incident Type") {
            return true
        }
        
        return false
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        mapView.clear()
        
        let marker = GMSMarker(position: coordinate)
        marker.title = "Incident Location"
        marker.map = mapView
        
        selectedLat = coordinate.latitude.description
        selectedLong = coordinate.longitude.description
        
    }
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Add the blur effect to the main View
        let blurEffect = UIBlurEffect(style: .regular)
        blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.view.frame
        self.view.addSubview(blurEffectView)
        self.blurEffectView.isHidden = true
        
        self.view.addSubview(incidentTypeView)
        incidentTypeView.center = self.view.center
        incidentTypeView.isHidden = true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        textView.resignFirstResponder()
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        switch(section){
        case 0:
            return 3
        case 1:
            return 2
        case 2:
            return 1
        default:
            return 0
        }
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        if(indexPath.section == 0){
            return 44
        }else if(indexPath.section == 1){
            if(indexPath.row == 0){
                return 44
            }else{
                return 131
            }
        }else{
            return 183
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        switch(indexPath.section){
        case 0:
            
            break
        case 1:
            if(indexPath.row == 0){
                self.view.bringSubview(toFront: blurEffectView)
                self.view.bringSubview(toFront: incidentTypeView)
                self.tableView.isScrollEnabled = false
                blurEffectView.isHidden = false
                incidentTypeView.isHidden = false
            }
            break
        case 2:
            break
        default: break
        }
    }
    
    func loadUserData(){
        var uid = ""
        let ref2 = ref.child("users").child(org)
        
        if let user = Auth.auth().currentUser{
            uid = user.uid
            emailField.text = user.email!
        }
        
        //TET THIS STUFF
        ref2.queryOrdered(byChild: "email").observe(.childAdded, with: { snapshot in
            if(snapshot.key == uid){
                let value = snapshot.value as! [String:AnyObject]
                if let pn = value["phone_number"] as? String {
                    self.numberField.text = pn
                }
                if let name = value["full_name"] as? String {
                    self.nameField.text = name
                }
            }
        })
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        incidentTypeLabel.text = "Incident Type: " + incidentTypes[row]
        selectedType = incidentTypes[row]
    }
    
    @IBAction func pickIncidentType(_ sender: Any) {
        self.tableView.isScrollEnabled = true
        blurEffectView.isHidden = true
        incidentTypeView.isHidden = true
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return incidentTypes[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return incidentTypes.count
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
}
