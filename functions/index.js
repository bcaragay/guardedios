/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();

/**
 * Triggers when a user gets a new follower and sends a notification.
 *
 * Followers add a flag to `/followers/{followedUid}/{followerUid}`.
 * Users save their device notification tokens to `/users/{followedUid}/notificationTokens/{notificationToken}`.
 */

exports.sendMessageNotification = functions.database.ref('Announcements/{schoolID}/{announcement}').onCreate((snapshot, context) => {
      // Grab the current value of what was written to the Realtime Database.
    const announcementBody = snapshot.val();
    const groupID = context.params.schoolID;
    
    var message = {
//        token:"dXmBlzn_yh0:APA91bHU9vo-mQUc45pI87Jtku6LaXmjq28q790VEWhMG8eEd3AFSt497QvY75CKf1F2rM3kEebN2tWLc_cOVcHfqWNjG9W9ykzOqltVGdHGWaTyVXGFDROv_aZ668XPDDNDATdMNsG1",
        notification: {
            title: "Guarded Alert",
            body: announcementBody,
        },
        data: {
            score: '850',
            time: '2:45'
        }
    };
    
   return admin.messaging().sendToTopic(groupID, message)
        .then(function(response) {
          console.log("Successfully sent message:", response);
        })
        .catch(function(error) {
          console.log("Error sending message:", error);
        });
  });